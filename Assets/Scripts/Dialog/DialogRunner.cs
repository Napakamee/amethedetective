using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text;
using CsvHelper;
using CsvHelper.Configuration;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using Yarn;
using Yarn.Unity;
using StringInfo = Yarn.Compiler.StringInfo;


namespace Dialog
{
    /// <summary>
    /// Combined YarnSpinner's <see cref="DialogueRunner"/> and <see cref="DialogueUI"/> to one script.
    /// Using this as base class for <see cref="DialogBase"/>.
    /// </summary>
    [RequireComponent(typeof(DialogUI))]
    [RequireComponent(typeof(InMemoryVariableStorage))]
    public class DialogRunner : MonoBehaviour, ILineLocalisationProvider
    {
        [SerializeField][ReadOnly] private DialogUI dialogUI;
        
        #region Private/Protected

        private string startNode = Yarn.Dialogue.DEFAULT_START;
        private string textLanguage;
        
        [SerializeField][ReadOnly] private VariableStorageBehaviour variableStorage;
        

        #endregion

        #region Public

        public bool IsDialogueRunning { get; set; }
        
        public string CurrentNodeName => Dialogue.currentNode;
        public Dialogue Dialogue => dialogue ??= CreateDialogueInstance();

        public DialogUI DialogUI
        {
            get => dialogUI;
            set => dialogUI = value;
        }

        #endregion
        
        #region Callback
        
        
        public Action<string> OnNodeStart;
        
        public Action<string> OnNodeComplete;
        
        public Action OnDialogComplete;

        

        #endregion

        private void OnValidate()
        {
            variableStorage = gameObject.GetComponent<InMemoryVariableStorage>();
            dialogUI = gameObject.GetComponent<DialogUI>();
        }

        #region DialogueRunner

        /// <summary>
        /// Adds a program, and parses and adds the contents of the
        /// program's string table to the DialogueRunner's combined string table.
        /// </summary>
        public void Add(YarnProgram _scriptToLoad)
        {
            Dialogue.AddProgram(_scriptToLoad.GetProgram());
            AddStringTable(_scriptToLoad);
        }
        
        /// <summary>
        /// Parses and adds the contents of a string table from a yarn
        /// asset to the DialogueRunner's combined string table.
        /// </summary>
        public void AddStringTable(YarnProgram _yarnScript)
        {
            var _textToLoad = new TextAsset();

            if (_yarnScript.localizations != null || _yarnScript.localizations.Length > 0) {
                _textToLoad = Array.Find(_yarnScript.localizations, _element => _element.languageName == textLanguage)?.text;
            }

            if (_textToLoad == null || string.IsNullOrEmpty(_textToLoad.text)) {
                _textToLoad = _yarnScript.baseLocalisationStringTable;
            }

            // Use the invariant culture when parsing the CSV
            var _configuration = new Configuration(
                CultureInfo.InvariantCulture
            );

            using (var _reader = new StringReader(_textToLoad.text))
            using (var _csv = new CsvReader(_reader, _configuration)) {
                _csv.Read();
                _csv.ReadHeader();

                while (_csv.Read())
                {
                    if (strings.ContainsKey(_csv.GetField("id")))
                    {
                        Debug.LogWarningFormat("Attempting to add {0} - {1} into DialogRunner, but it already exists!", _csv.GetField("id"), _csv.GetField("text"));
                        return;
                    }
                    strings.Add(_csv.GetField("id"), _csv.GetField("text"));
                }
            }
        }
        
        /// <summary>
        /// Adds entries to the DialogueRunner's combined string table.
        /// </summary>
        public void AddStringTable(IDictionary<string, StringInfo> _stringTable)
        {
            foreach (var _line in _stringTable) {
                strings.Add(_line.Key, _line.Value.text);
            }
        }
        
        /// <summary>
        /// Starts running dialogue. The node specified by <see
        /// cref="startNode"/> will start running.
        /// </summary>
        public void StartDialogue() => StartDialogue(startNode);
        
        /// <summary>
        /// Start the dialogue from a specific node.
        /// </summary>
        public void StartDialogue(string _startNode)
        {
            // Stop any processes that might be running already
            StopAllCoroutines();

            // Get it going
            RunDialogue();
            void RunDialogue()
            {
                // Mark that we're in conversation.
                IsDialogueRunning = true;

                // Signal that we're starting up.
                dialogUI.DialogueStarted();

                Dialogue.SetNode(_startNode);

                ContinueDialogue();
            }
        }
        
        /// <summary>
        /// Resets the <see cref="variableStorage"/>, and starts running the dialogue again from the node named <see cref="startNode"/>.
        /// </summary>        
        public void ResetDialogue()
        {
            variableStorage.ResetToDefaults();
            StartDialogue();
        }
        
        /// <summary>
        /// Unloads all nodes from the <see cref="dialogue"/>.
        /// </summary>
        public void Clear()
        {
            Assert.IsFalse(IsDialogueRunning, "You cannot clear the dialogue system while a dialogue is running.");
            Dialogue.UnloadAll();
        }
        
        /// <summary>
        /// Stops the <see cref="dialogue"/>.
        /// </summary>
        public void Stop()
        {
            IsDialogueRunning = false;
            Dialogue.Stop();
        }
        
        /// <summary>
        /// Returns `true` when a node named `nodeName` has been loaded.
        /// </summary>
        /// <param name="_nodeName">The name of the node.</param>
        /// <returns>`true` if the node is loaded, `false` otherwise/</returns>
        public bool NodeExists(string _nodeName) => Dialogue.NodeExists(_nodeName);
        
        /// <summary>
        /// Returns the collection of tags that the node associated with
        /// the node named `nodeName`.
        /// </summary>
        /// <param name="_nodeName">The name of the node.</param>
        /// <returns>The collection of tags associated with the node, or
        /// `null` if no node with that name exists.</returns>
        public IEnumerable<string> GetTagsForNode(String _nodeName) => Dialogue.GetTagsForNode(_nodeName);
        
        
        /// <summary>
        /// Adds a command handler. Dialogue will continue running after the command is called.
        /// </summary>
        /// <remarks>
        /// When this command handler has been added, it can be called from
        /// your Yarn scripts like so:
        ///
        /// <![CDATA[
        /// ```yarn
        /// <<commandName param1 param2>>
        /// ```
        /// ]]>
        ///
        /// When this command handler is called, the DialogueRunner will
        /// not stop executing code.
        /// </remarks>
        /// <param name="_commandName">The name of the command.</param>
        /// <param name="_handler">The <see cref="CommandHandler"/> that will be invoked when the command is called.</param>
        public void AddCommandHandler(string _commandName, CommandHandler _handler)
        {
            if (commandHandlers.ContainsKey(_commandName) || blockingCommandHandlers.ContainsKey(_commandName)) {
                Debug.LogError($"Cannot add a command handler for {_commandName}: one already exists");
                return;
            }
            commandHandlers.Add(_commandName, _handler);
        }
        
        public void AddCommandHandler(string _commandName, BlockingCommandHandler _handler)
        {
            if (commandHandlers.ContainsKey(_commandName) || blockingCommandHandlers.ContainsKey(_commandName)) {
                Debug.LogError($"Cannot add a command handler for {_commandName}: one already exists");
                return;
            }
            blockingCommandHandlers.Add(_commandName, _handler);
        }
        
        /// <summary>
        /// Removes a command handler.
        /// </summary>
        /// <param name="_commandName">The name of the command to remove.</param>
        public void RemoveCommandHandler(string _commandName)
        {
            commandHandlers.Remove(_commandName);
            blockingCommandHandlers.Remove(_commandName);
        }
        
        /// <summary>
        /// Add a new function that returns a value, so that it can be
        /// called from Yarn scripts.
        /// </summary>        
        /// <inheritdoc cref="AddFunction(string, int, Function)"/>
        /// <remarks>
        /// If `parameterCount` is -1, the function expects any number of
        /// parameters.
        ///
        /// When this function has been registered, it can be called from
        /// your Yarn scripts like so:
        /// 
        /// <![CDATA[
        /// ```yarn
        /// <<if myFunction(1, 2) == true>>
        ///     myFunction returned true!
        /// <<endif>>
        /// ```
        /// ]]>
        /// 
        /// The `call` command can also be used to invoke the function:
        /// 
        /// <![CDATA[
        /// ```yarn
        /// <<call myFunction(1, 2)>>
        /// ```
        /// ]]>    
        /// </remarks>
        /// <param name="_implementation">The <see cref="ReturningFunction"/>
        /// that should be invoked when this function is called.</param>
        /// <seealso cref="AddFunction(string, int, Function)"/>
        /// <seealso cref="AddFunction(string, int, ReturningFunction)"/>
        /// <seealso cref="Library"/> 
        /// <inheritdoc cref="AddFunction(string, int, Function)"/>       
        public void AddFunction(string _name, int _parameterCount, ReturningFunction _implementation)
        {
            if (Dialogue.library.FunctionExists(_name)) {
                Debug.LogError($"Cannot add function {_name} one already exists");
                return;
            }

            Dialogue.library.RegisterFunction(_name, _parameterCount, _implementation);
        }
        
        
        /// <summary>
        /// Add a new function, so that it can be called from Yarn scripts.
        /// </summary>
        /// <remarks>
        /// If `parameterCount` is -1, the function expects any number of
        /// parameters.
        ///
        /// When this function has been registered, it can be invoked using
        /// the `call` command:
        ///
        /// <![CDATA[
        /// ```yarn
        /// <<call myFunction(1, 2)>>
        /// ```
        /// ]]>    
        /// </remarks>
        /// <param name="_name">The name of the function to add.</param>
        /// <param name="_parameterCount">The number of parameters that this
        /// function expects.</param>
        /// <param name="_implementation">The <see cref="Function"/>
        /// that should be invoked when this function is called.</param>
        /// <seealso cref="AddFunction(string, int, Function)"/>
        /// <seealso cref="AddFunction(string, int, ReturningFunction)"/>
        /// <seealso cref="Library"/> 
        public void AddFunction(string _name, int _parameterCount, Function _implementation)
        {
            if (Dialogue.library.FunctionExists(_name)) {
                Debug.LogError($"Cannot add function {_name} one already exists");
                return;
            }

            Dialogue.library.RegisterFunction(_name, _parameterCount, _implementation);
        }
        
        /// <summary>
        /// Remove a registered function.
        /// </summary>
        /// <remarks>
        /// After a function has been removed, it cannot be called from Yarn scripts.
        /// </remarks>
        /// <param name="_name">The name of the function to remove.</param>
        /// <seealso cref="AddFunction(string, int, Function)"/>
        /// <seealso cref="AddFunction(string, int, ReturningFunction)"/>
        public void RemoveFunction(string _name) => Dialogue.library.DeregisterFunction(_name);
        
        #endregion
        
        #region Private Properties/Variables/Procedures

        Action continueAction;
        Action<int> selectAction;

        /// <summary>
        /// Represents a method that can be called when the DialogueRunner
        /// encounters a command. 
        /// </summary>
        /// <remarks>
        /// After this method returns, the DialogueRunner will continue
        /// executing code.
        /// </remarks>
        /// <param name="_parameters">The list of parameters that this
        /// command was invoked with.</param>
        /// <seealso cref="AddCommandHandler(string, CommandHandler)"/>
        /// <seealso cref="AddCommandHandler(string,
        /// BlockingCommandHandler)"/>
        public delegate void CommandHandler(string[] _parameters);

        /// <summary>
        /// Represents a method that can be called when the DialogueRunner
        /// encounters a command. 
        /// </summary>
        /// <remarks>
        /// After this method returns, the DialogueRunner will pause
        /// executing code. The `onComplete` delegate will cause the
        /// DialogueRunner to resume executing code.
        /// </remarks>
        /// <param name="_parameters">The list of parameters that this
        /// command was invoked with.</param>
        /// <param name="_onComplete">The method to call when the DialogueRunner should continue executing code.</param>
        /// <seealso cref="AddCommandHandler(string, CommandHandler)"/>
        /// <seealso cref="AddCommandHandler(string, BlockingCommandHandler)"/>
        public delegate void BlockingCommandHandler(string[] _parameters, Action _onComplete);

        /// Maps the names of commands to action delegates.
        Dictionary<string, CommandHandler> commandHandlers = new Dictionary<string, CommandHandler>();
        Dictionary<string, BlockingCommandHandler> blockingCommandHandlers = new Dictionary<string, BlockingCommandHandler>();

        // Maps string IDs received from Yarn Spinner to user-facing text
        Dictionary<string, string> strings = new Dictionary<string, string>();

        // A flag used to note when we call into a blocking command
        // handler, but it calls its complete handler immediately -
        // _before_ the Dialogue is told to pause. This out-of-order
        // problem can lead to the Dialogue being stuck in a paused state.
        // To solve this, this variable is set to false before any blocking
        // command handler is called, and set to true when ContinueDialogue
        // is called. If it's true after calling a blocking command
        // handler, then the Dialogue is not told to pause.
        bool wasCompleteCalled = false;

        /// Our conversation engine
        /** Automatically created on first access
         */
        Dialogue dialogue;

        private void Awake()
        {
            if (!variableStorage)
                variableStorage = gameObject.GetComponent<InMemoryVariableStorage>();
            
            if (!dialogUI)
                dialogUI = gameObject.GetComponent<DialogUI>();
            
        }

        /// Start the dialogue
        void Start()
        {
            //Assert.IsNotNull(dialogueUIBehaviour, "Implementation was not set! Can't run the dialogue!");
            Assert.IsNotNull(variableStorage, "Variable storage was not set! Can't run the dialogue!");

            // Ensure that the variable storage has the right stuff in it
            variableStorage.ResetToDefaults();
            
        }

        Dialogue CreateDialogueInstance()
        {
            // Create the main Dialogue runner, and pass our
            // variableStorage to it
            var _dialogue = new Yarn.Dialogue(variableStorage) {

                // Set up the logging system.
                LogDebugMessage = delegate (string _message) {
                    Debug.Log(_message);
                },
                LogErrorMessage = delegate (string _message) {
                    Debug.LogError(_message);
                },

                lineHandler = HandleLine,
                commandHandler = HandleCommand,
                optionsHandler = HandleOptions,
                nodeStartHandler = (_node) => {
                    OnNodeStart?.Invoke(_node);
                    return Dialogue.HandlerExecutionType.ContinueExecution;
                },
                nodeCompleteHandler = (_node) => {
                    OnNodeComplete?.Invoke(_node);
                    return Dialogue.HandlerExecutionType.ContinueExecution;
                },
                dialogueCompleteHandler = HandleDialogueComplete
            };

            // Yarn Spinner defines two built-in commands: "wait",
            // and "stop". Stop is defined inside the Virtual
            // Machine (the compiler traps it and makes it a
            // special case.) Wait is defined here in Unity.
            AddCommandHandler("wait", HandleWaitCommand);
            
            continueAction = ContinueDialogue;
            selectAction = SelectedOption;

            return _dialogue;

            void HandleWaitCommand(string[] _parameters, Action _onComplete)
            {
                if (_parameters?.Length != 1) {
                    Debug.LogErrorFormat("<<wait>> command expects one parameter.");
                    _onComplete();
                    return;
                }

                string _durationString = _parameters[0];

                if (float.TryParse(_durationString,
                                   System.Globalization.NumberStyles.AllowDecimalPoint,
                                   System.Globalization.CultureInfo.InvariantCulture,
                                   out var _duration) == false) {

                    Debug.LogErrorFormat($"<<wait>> failed to parse duration {_durationString}");
                    _onComplete();
                }

                StartCoroutine(DoHandleWait());
                IEnumerator DoHandleWait()
                {
                    yield return new WaitForSeconds(_duration);
                    _onComplete();
                }
            }

            void HandleOptions(OptionSet _options) => dialogUI.RunOptions(_options, this, selectAction);

            void HandleDialogueComplete()
            {
                IsDialogueRunning = false;
                dialogUI.DialogueComplete();
                OnDialogComplete.Invoke();
            }
            
            

            Dialogue.HandlerExecutionType HandleCommand(Command _command)
            {
                bool _wasValidCommand;
                Dialogue.HandlerExecutionType _executionType;

                // Try looking in the command handlers first, which is a lot
                // cheaper than crawling the game object hierarchy.

                // Set a flag that we can use to tell if the dispatched command
                // immediately called _continue
                wasCompleteCalled = false;

                (_wasValidCommand, _executionType) = DispatchCommandToRegisteredHandlers(_command, continueAction);

                if (_wasValidCommand) {

                    // This was a valid command. It returned either continue,
                    // or pause; if it returned pause, there's a chance that
                    // the command handler immediately called _continue, in
                    // which case we should not pause.
                    if (wasCompleteCalled) {
                        return Dialogue.HandlerExecutionType.ContinueExecution;
                    }
                    else {
                        // Either continue execution, or pause (in which case
                        // _continue will be called)
                        return _executionType;
                    }
                }

                // We didn't find it in the comand handlers. Try looking in the game objects.
                (_wasValidCommand, _executionType) = DispatchCommandToGameObject(_command);

                if (_wasValidCommand) {
                    // We found an object and method to invoke as a Yarn
                    // command. It may or may not have been a coroutine; if it
                    // was a coroutine, executionType will be
                    // HandlerExecutionType.Pause, and we'll wait for it to
                    // complete before resuming execution.
                    return _executionType;
                }

                // We didn't find a method in our C# code to invoke. Pass it to
                // the UI to handle; it will determine whether we pause or
                // continue.
                return dialogUI.RunCommand(_command, continueAction);
            }

            /// Forward the line to the dialogue UI.
            Dialogue.HandlerExecutionType HandleLine(Line _line) => dialogUI.RunLine(_line, this, continueAction);

            /// Indicates to the DialogueRunner that the user has selected an option
            void SelectedOption(int _obj)
            {
                Dialogue.SetSelectedOption(_obj);
                ContinueDialogue();
            }

            (bool commandWasFound, Dialogue.HandlerExecutionType executionType) DispatchCommandToRegisteredHandlers(Command command, Action onComplete)
            {
                var _commandTokens = command.Text.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                //Debug.Log($"Command: <<{command.Text}>>");

                if (_commandTokens.Length == 0) {
                    // Nothing to do
                    return (false, Dialogue.HandlerExecutionType.ContinueExecution);
                }

                var _firstWord = _commandTokens[0];

                if (commandHandlers.ContainsKey(_firstWord) == false &&
                    blockingCommandHandlers.ContainsKey(_firstWord) == false) {

                    // We don't have a registered handler for this command, but
                    // some other part of the game might.
                    return (false, Dialogue.HandlerExecutionType.ContinueExecution);
                }

                // Single-word command, eg <<jump>>
                if (_commandTokens.Length == 1) {
                    if (commandHandlers.ContainsKey(_firstWord)) {
                        commandHandlers[_firstWord](null);
                        return (true, Dialogue.HandlerExecutionType.ContinueExecution);
                    }
                    else {
                        blockingCommandHandlers[_firstWord](new string[] { }, onComplete);
                        return (true, Dialogue.HandlerExecutionType.PauseExecution);
                    }
                }

                // Multi-word command, eg <<walk Mae left>>
                var _remainingWords = new string[_commandTokens.Length - 1];

                // Copy everything except the first word from the array
                System.Array.Copy(_commandTokens, 1, _remainingWords, 0, _remainingWords.Length);

                if (commandHandlers.ContainsKey(_firstWord)) {
                    commandHandlers[_firstWord](_remainingWords);
                    return (true, Dialogue.HandlerExecutionType.ContinueExecution);
                }
                else {
                    blockingCommandHandlers[_firstWord](_remainingWords, onComplete);
                    return (true, Dialogue.HandlerExecutionType.PauseExecution);
                }
            }

            /// commands that can be automatically dispatched look like this:
            /// COMMANDNAME OBJECTNAME <param> <param> <param> ...
            /** We can dispatch this command if:
             * 1. it has at least 2 words
             * 2. the second word is the name of an object
             * 3. that object has components that have methods with the
             *    YarnCommand attribute that have the correct commandString set
             */
            (bool methodFound, Dialogue.HandlerExecutionType executionType) DispatchCommandToGameObject(Command command)
            {
                var _words = command.Text.Split(' ');

                // need 2 parameters in order to have both a command name
                // and the name of an object to find
                if (_words.Length < 2) {
                    return (false, Dialogue.HandlerExecutionType.ContinueExecution);
                }

                var _commandName = _words[0];

                var _objectName = _words[1];

                var _sceneObject = GameObject.Find(_objectName);

                // If we can't find an object, we can't dispatch a command
                if (_sceneObject == null) {
                    return (false, Dialogue.HandlerExecutionType.ContinueExecution);
                }

                int _numberOfMethodsFound = 0;
                List<string[]> _errorValues = new List<string[]>();

                List<string> _parameters;

                if (_words.Length > 2) {
                    _parameters = new List<string>(_words);
                    _parameters.RemoveRange(0, 2);
                }
                else {
                    _parameters = new List<string>();
                }

                var _startedCoroutine = false;

                // Find every MonoBehaviour (or subclass) on the object
                foreach (var _component in _sceneObject.GetComponents<MonoBehaviour>()) {
                    var _type = _component.GetType();

                    // Find every method in this component
                    foreach (var _method in _type.GetMethods()) {

                        // Find the YarnCommand attributes on this method
                        var _attributes = (YarnCommandAttribute[])_method.GetCustomAttributes(typeof(YarnCommandAttribute), true);

                        // Find the YarnCommand whose commandString is equal to
                        // the command name
                        foreach (var _attribute in _attributes) {
                            if (_attribute.CommandString == _commandName) {

                                var _methodParameters = _method.GetParameters();
                                bool _paramsMatch = false;
                                // Check if this is a params array
                                if (_methodParameters.Length == 1 &&
                                    _methodParameters[0].ParameterType.IsAssignableFrom(typeof(string[]))) {
                                    // Cool, we can send the command!

                                    // If this is a coroutine, start it,
                                    // and set a flag so that we know to
                                    // wait for it to finish
                                    string[][] _paramWrapper = new string[1][];
                                    _paramWrapper[0] = _parameters.ToArray();
                                    if (_method.ReturnType == typeof(IEnumerator)) {
                                        StartCoroutine(DoYarnCommandParams(_component, _method, _paramWrapper));
                                        _startedCoroutine = true;
                                    }
                                    else {
                                        _method.Invoke(_component, _paramWrapper);

                                    }
                                    _numberOfMethodsFound++;
                                    _paramsMatch = true;

                                }
                                // Otherwise, verify that this method has the right number of parameters
                                else if (_methodParameters.Length == _parameters.Count) {
                                    _paramsMatch = true;
                                    foreach (var _paramInfo in _methodParameters) {
                                        if (!_paramInfo.ParameterType.IsAssignableFrom(typeof(string))) {
                                            Debug.LogErrorFormat(_sceneObject, "Method \"{0}\" wants to respond to Yarn command \"{1}\", but not all of its parameters are strings!", _method.Name, _commandName);
                                            _paramsMatch = false;
                                            break;
                                        }
                                    }
                                    if (_paramsMatch) {
                                        // Cool, we can send the command!

                                        // If this is a coroutine, start it,
                                        // and set a flag so that we know to
                                        // wait for it to finish
                                        if (_method.ReturnType == typeof(IEnumerator)) {
                                            StartCoroutine(DoYarnCommand(_component, _method, _parameters.ToArray()));
                                            _startedCoroutine = true;
                                        }
                                        else {
                                            _method.Invoke(_component, _parameters.ToArray());
                                        }
                                        _numberOfMethodsFound++;
                                    }
                                }
                                //parameters are invalid, but name matches.
                                if (!_paramsMatch) {
                                    //save this error in case a matching
                                    //command is never found.
                                    _errorValues.Add(new string[] { _method.Name, _commandName, _methodParameters.Length.ToString(), _parameters.Count.ToString() });
                                }
                            }
                        }
                    }
                }

                // Warn if we found multiple things that could respond to this
                // command.
                if (_numberOfMethodsFound > 1) {
                    Debug.LogWarningFormat(_sceneObject, "The command \"{0}\" found {1} targets. " +
                        "You should only have one - check your scripts.", command, _numberOfMethodsFound);
                }
                else if (_numberOfMethodsFound == 0) {
                    //list all of the near-miss methods only if a proper match
                    //is not found, but correctly-named methods are.
                    foreach (string[] _errorVal in _errorValues) {
                        Debug.LogErrorFormat(_sceneObject, "Method \"{0}\" wants to respond to Yarn command \"{1}\", but it has a different number of parameters ({2}) to those provided ({3}), or is not a string array!", _errorVal[0], _errorVal[1], _errorVal[2], _errorVal[3]);
                    }
                }

                var _wasValidCommand = _numberOfMethodsFound > 0;

                if (_wasValidCommand == false) {
                    return (false, Dialogue.HandlerExecutionType.ContinueExecution);
                }

                if (_startedCoroutine) {
                    // Signal to the Dialogue that execution should wait. 
                    return (true, Dialogue.HandlerExecutionType.PauseExecution);
                }
                else {
                    // This wasn't a coroutine, so no need to wait for it.
                    return (true, Dialogue.HandlerExecutionType.ContinueExecution);
                }

                IEnumerator DoYarnCommandParams(MonoBehaviour _component,
                                                MethodInfo _method,
                                                string[][] _localParameters)
                {
                    // Wait for this command coroutine to complete
                    yield return StartCoroutine((IEnumerator)_method.Invoke(_component, _localParameters));

                    // And then continue running dialogue
                    ContinueDialogue();
                }

                IEnumerator DoYarnCommand(MonoBehaviour _component,
                                          MethodInfo _method,
                                          string[] _localParameters)
                {
                    // Wait for this command coroutine to complete
                    yield return StartCoroutine((IEnumerator)_method.Invoke(_component, _localParameters));

                    // And then continue running dialogue
                    ContinueDialogue();
                }
            }
        }

        void ContinueDialogue()
        {
            wasCompleteCalled = true;
            Dialogue.Continue();
        }
        

        #endregion
        
        
        
        string ILineLocalisationProvider.GetLocalisedTextForLine(Line _line)
        {
            if (!strings.TryGetValue(_line.ID, out var _result)) return null;

            // Now that we know the localised string for this line, we
            // can go ahead and inject this line's substitutions.
            for (int i = 0; i < _line.Substitutions.Length; i++) {
                string _substitution = _line.Substitutions[i];
                _result = _result.Replace("{" + i + "}", _substitution);
            }

            // Apply in-line format functions
            _result = Dialogue.ExpandFormatFunctions(_result, textLanguage);

            return _result;
        }
        
    }
}
