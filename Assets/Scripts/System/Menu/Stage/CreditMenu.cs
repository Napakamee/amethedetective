using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditMenu : MonoBehaviour
{
    private Canvas canvas;

    public void OnClick_Back()
    {
        canvas = gameObject.GetComponent<Canvas>();
        GameMenuManager.OpenMenu(Menu.MAINMENU, canvas);
    }
}
