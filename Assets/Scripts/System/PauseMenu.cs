using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{

    [SerializeField] private Button pauseButton;
    [SerializeField] private Button resumeButton;
    [SerializeField] private Canvas pauseMenu;
    
    
    private void Start()
    {
        var _inputManager = FindObjectOfType<InputManager>();
        _inputManager.OnEsc += _context =>
        {
            ToggleMenu();
        };
        Resume();
        
        pauseMenu.enabled = false;
        pauseButton.onClick.AddListener(Pause);
        resumeButton.onClick.AddListener(Resume);
    }

    public void ToggleObject(GameObject _gameObject)
    {
        _gameObject.SetActive(!_gameObject.activeSelf);
    }

    private void ToggleMenu()
    {
        if (GameManager.Instance.GameIsPaused)
        {
            Resume();
        }
        else
        {
            if (!GameManager.Instance.IsInteracting)
            {
                Pause();
            }
                
        }
    }

    public void Pause()
    {
        pauseMenu.enabled = true;
        Time.timeScale = 0f;
        GameManager.Instance.GameIsPaused = true;
    }

    public void Resume()
    {
        pauseMenu.enabled = false;
        Time.timeScale = 1f;
        GameManager.Instance.GameIsPaused = false;
    }
}
