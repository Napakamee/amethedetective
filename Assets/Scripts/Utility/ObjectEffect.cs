using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public static class ObjectEffect 
{
    public static void Shake(this Transform _transform, Tweener _tweener = null)
    {
        _tweener.Kill();
        _tweener = _transform.DOShakePosition(0.08f, 0.1f);
        _tweener.Play();
    }

    public static void Blink(this SpriteRenderer _sprite, float _duration = 1f, float _blinkDuration = 0.2f)
    {
        Sequence _sequence = DOTween.Sequence();
        int _loop = (int)(_duration / (_blinkDuration * 2));
        for (int i = 0; i < _loop; i++)
        {
            _sequence.Append(_sprite.DOFade(0.5f, _blinkDuration));
            _sequence.Append(_sprite.DOFade(1f, _blinkDuration));
        }
        
    }

    public static void Flash(this SpriteRenderer _sprite, Color _flashColor, float _duration = 0.1f)
    {
        Sequence _sequence = DOTween.Sequence();

        _sequence.Append(_sprite.DOColor(_flashColor, _duration));
        _sequence.Append(_sprite.DOColor(Color.white, 0.1f));
    }
}
