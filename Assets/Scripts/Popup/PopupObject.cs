using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupObject : PopupBase
{
    [SerializeField] private GameObject container;
    [SerializeField] private Button exitButton;
    [SerializeField] private Animator animator;
    [SerializeField] private AnimationClip inoutClip;

    private WaitForSeconds waitAnim;
    private bool isActive = false;
    
    public void Init()
    {
        waitAnim = new WaitForSeconds(inoutClip ? inoutClip.length : 0);
        exitButton.onClick.AddListener(Close);
        var _inputManager = FindObjectOfType<InputManager>();
        _inputManager.OnEsc += _context =>
        {
            if (isActive)
            {
                Close();
            }
        };
        
        container.SetActive(false);
    }

    public void Open()
    {
        container.SetActive(true);
        isActive = true;
        OnOpen?.Invoke();
    }

    private void Close()
    {
        container.SetActive(false);
        isActive = false;
        OnClose?.Invoke();
    }

    private IEnumerator IEOpen(Action _onComplete = null)
    {
        //Play animation
        yield return waitAnim;
    }

    private IEnumerator IEClose(Action _onComplete = null)
    {
        //Play animation
        yield return waitAnim;
    }
}
