using System;
using System.Collections;
using System.Collections.Generic;
using Module.Interact;
using RedBlueGames.Tools.TextTyper;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;


public class DialogPlayer : PopupBase
{
    #region Inspector

    [SerializeField] private DialogData dialogData;
    
    [SerializeField] private Color tiltColor;
    [SerializeField] private AudioClip textSound;
    [SerializeField] private Button skipButton;
    [SerializeField] private RectTransform charMain;
    [SerializeField] private RectTransform charSide;
    [SerializeField] private RectTransform cutScene;
    [SerializeField] private TextMeshProUGUI nameTMP;
    #endregion
    
    
    #region Private

    private Dictionary<string, Vector2> charPos = new Dictionary<string, Vector2>()
    {
        {"left", new Vector2(0.25f,0.5f)},
        {"middle", new Vector2(0.5f,0.5f)},
        {"right", new Vector2(0.75f, 0.5f)}
    };
    
    private readonly Queue<DialogText> dialogueLines = new Queue<DialogText>();
    private TextTyper textTyper;
    private InteractShowDialog currentObject;
    private Canvas canvas;
    private bool isPlaying = false;
    
    #endregion
    public void Init()
    {
        canvas = GetComponent<Canvas>();
        canvas.enabled = false;
        
        textTyper = GetComponentInChildren<TextTyper>();
        if (textTyper == null)
        {
            Debug.LogError("Text typer is missing");
        }
        
        skipButton.onClick.AddListener(SkipDialog);
        
        textTyper.PrintCompleted.AddListener(FinishedDialog);
        textTyper.CharacterPrinted.AddListener(PrintedText);
    }

    private void AddCommand()
    {
        // runner.AddCommandHandler<string>("Scene", DoSceneChange );
        // runner.AddCommandHandler<string,string,string,string,string>("Act", SetActor );
        // runner.AddCommandHandler<string,string,string>("Draw", SetSpriteYarn );
        //
        // runner.AddCommandHandler<string>("Hide", HideSprite );
        // runner.AddCommandHandler("HideAll", HideAllSprites );
        // runner.AddCommandHandler("Reset", ResetScene );
        //
        // runner.AddCommandHandler<string,string,string,float>("Move", MoveSprite );
        // runner.AddCommandHandler<string,string>("Flip", FlipSprite );
        // runner.AddCommandHandler<string,float>("Shake", ShakeSprite );
        //
        // runner.AddCommandHandler<string,float,string>("PlayAudio", PlayAudio );
        // runner.AddCommandHandler<string>("StopAudio", StopAudio );
        // runner.AddCommandHandler("StopAudioAll", StopAudioAll );
        //
        // runner.AddCommandHandler<string,float,float,float>("Fade", SetFade );
        // runner.AddCommandHandler<float>("FadeIn", SetFadeIn );
        // runner.AddCommandHandler<string,string,float>("CamOffset", SetCameraOffset );
    }
    

    public void PlayDialog(DialogTextData _dialogTextData)
    {
        dialogueLines.Clear();
        
        canvas.enabled = true;
        isPlaying = true;
        
        foreach (var _dialog in _dialogTextData.Dialog)
        {
            dialogueLines.Enqueue(_dialog);
        }
        ShowScript();
        OnOpen?.Invoke();
    }

    public void PlayDialog(DialogTextData _dialogTextData, InteractShowDialog _obj)
    {
        if (currentObject == _obj) return;
        currentObject = _obj;
        
        PlayDialog(_dialogTextData);
    }
    
    private void ShowScript()
    {
        if (dialogueLines.Count <= 0)
        {
            return;
        }
        
        //ShowSprite();

        if (dialogueLines.Peek().mainCharacter.Name != null)
        {
            nameTMP.text = dialogueLines.Peek().mainCharacter.Name;
        }
        else
        {
            nameTMP.text = "";
        }
        
        textTyper.TypeText(dialogueLines.Dequeue().text);
    }
    
    private void SkipDialog()
    {
        if (dialogueLines.Count <= 0 && !isPlaying)
        {
            canvas.enabled = false;
            OnClose?.Invoke();
        }
        
        if (textTyper.IsSkippable() && textTyper.IsTyping)
        {
            textTyper.Skip();
        }
        else
        {
            ShowScript();
        }
        
    }

    // private void ShowSprite()
    // {
    //     //Set sprite
    //     if (dialogueLines.Peek().mainCharacter.Sprite != null)
    //     {
    //         SetSprite(charMain, dialogueLines.Peek().mainCharacter.Sprite
    //             , dialogueLines.Peek().mainCharPosition, Color.white);
    //     }
    //     else
    //     {
    //         charMain.GetComponent<Image>().enabled = false;
    //     }
    //     
    //     //SideChar
    //     if ((dialogueLines.Peek().dialogFeature & DialogFeature.SideChar) != 0)
    //     {
    //         if (dialogueLines.Peek().sideCharacter.Sprite != null)
    //         {
    //             SetSprite(charSide, dialogueLines.Peek().sideCharacter.Sprite
    //                 , dialogueLines.Peek().sideCharPosition, tiltColor);
    //         }
    //         else
    //         {
    //             charSide.GetComponent<Image>().enabled = false;
    //         }
    //     }
    //     else
    //     {
    //         charSide.GetComponent<Image>().enabled = false;
    //     }
    //     
    //     //Cutscene
    //     if ((dialogueLines.Peek().dialogFeature & DialogFeature.Cutscene) != 0)
    //     {
    //         if (dialogueLines.Peek().cutsceneSprite != null)
    //         {
    //             cutScene.GetComponent<Image>().sprite = dialogueLines.Peek().cutsceneSprite;
    //             cutScene.GetComponent<Image>().enabled = true;
    //         }
    //     }
    //     else
    //     {
    //         cutScene.GetComponent<Image>().enabled = false;
    //     }
    // }
    
    private void SetSpeakerPos(string[] _param)
    {
        var _char = FetchAsset<RectTransform>(_param[0]);
        if(!_char) return;
        var _spritePos = _param[1];
        switch (_spritePos)
        {
            case "left": 
                _char.anchorMax = charPos["left"];
                _char.anchorMin = charPos["left"];
                    
                break;
            case "middle":
                _char.anchorMax = charPos["middle"];
                _char.anchorMin = charPos["middle"];
                break;
            case "right":
                _char.anchorMax = charPos["right"];
                _char.anchorMin = charPos["right"];
                break;
        }
        

        _char.anchoredPosition = new Vector2(0, -100);
    }

    // private void SetSprite(RectTransform _char, Sprite _sprite, SpritePosition _spritePos, Color _color)
    // {
    //     var _image = _char.GetComponent<Image>();
    //     _image.sprite = _sprite;
    //     _image.enabled = true;
    //     _image.color = _color;
    //     switch (_spritePos)
    //     {
    //         case SpritePosition.Left: 
    //             _char.anchorMax = charPos["left"];
    //             _char.anchorMin = charPos["left"];
    //                 
    //             break;
    //         case SpritePosition.Middle:
    //             _char.anchorMax = charPos["middle"];
    //             _char.anchorMin = charPos["middle"];
    //             break;
    //         case SpritePosition.Right:
    //             _char.anchorMax = charPos["right"];
    //             _char.anchorMin = charPos["right"];
    //             break;
    //     }
    //     
    //
    //     _char.anchoredPosition = new Vector2(0, -100);
    // }

    #region Callback

    private void FinishedDialog()
    {
        if (dialogueLines.Count <= 0)
        {
            isPlaying = false;
            currentObject = null;
        }
    }

    /// <summary>
    /// When finished each character.
    /// </summary>
    /// <param name="_printedCharacter"></param>
    private void PrintedText(string _printedCharacter)
    {
        // Do not play a sound for whitespace
        if (_printedCharacter == " " || _printedCharacter == "\n")
        {
            return;
        }
        
        var _audioSource = GetComponent<AudioSource>();
        if (_audioSource == null)
        {
            _audioSource = gameObject.AddComponent<AudioSource>();
        }

        if (textSound == null) return;
        _audioSource.clip = textSound;
        _audioSource.Play();
    }

    #endregion

    #region Utility

    // utility function to find an asset, whether it's in \Resources\
    // or manually loaded via an array
    T FetchAsset<T>( string assetName ) where T : UnityEngine.Object {
        // first, check to see if it's a manully loaded asset, with
        // manual array checks... it's messy but I can't think of a
        // better way to do this
        if ( typeof(T) == typeof(Sprite) ) {
            foreach ( var spr in dialogData.Sprites ) {
                if (spr.name == assetName) {
                    return spr as T;
                }
            }
        } else if ( typeof(T) == typeof(AudioClip) ) {
            foreach ( var ac in dialogData.Audio ) {
                if ( ac.name == assetName ) {
                    return ac as T;
                }
            }
        }

        // by default, we load all Resources assets into the asset
        // arrays already, but if you don't want that, then uncomment
        // this, etc. if ( useResourcesFolders ) {var newAsset =
        // Resources.Load<T>(assetName); if ( newAsset != null )
        // {return newAsset;
        //  }
        // }

        Debug.LogErrorFormat(this, "VN Manager can't find asset [{0}]... maybe it is misspelled, or isn't imported as {1}?", assetName, typeof(T).ToString() );
        return null; // didn't find any matching asset
    }

    #endregion
}
