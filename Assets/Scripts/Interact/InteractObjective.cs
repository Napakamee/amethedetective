using System;
using System.Collections;
using System.Collections.Generic;
using Module.Interact;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class InteractObjective : InteractableObject
{
    [SerializeField][InlineEditor] private ObjectiveData objectiveData;
    [SerializeField] private AudioClip completeSound;
    [SerializeField] public UnityEvent OnComplete;

    [Button]
    private void GenerateObjective()
    {
#if UNITY_EDITOR
        if (!objectiveData)
        {
            var _objectiveData = ScriptableObject.CreateInstance<ObjectiveData>();
            var _name = name + "Objective.asset";
            var _path = "Assets/Data/ObjectiveData/" + _name;
            AssetDatabase.CreateAsset(_objectiveData, _path);
            AssetDatabase.SaveAssets();
            objectiveData = _objectiveData;
            Debug.LogFormat(@"Created {0} at <color=green>{1}</color>", _name, _path);
        }

        objectiveData.GenerateObjectiveData(isUI ? 
                GetComponent<Image>().sprite :
                GetComponent<SpriteRenderer>().sprite,
            name);

        Debug.LogFormat(@"Set {0}'s sprite and name,<color=orange> you still need to manually set [ID] and [Description]!</color>",objectiveData.name);
#endif
    }
    private void Awake()
    {
        
    }

    public override void Init()
    {
        base.Init();
        objectiveData.Init();
        var _objectiveList = GameManager.Instance.InteractObjectives;
        if (!_objectiveList.Contains(objectiveData))
        {
            _objectiveList.Add(objectiveData);
            //Debug.Log($"Added {objectiveData.Name}");
        }
        GameManager.Instance.SortObjective();
    }

    public override void OnClick(int _inputID)
    {
        base.OnClick(_inputID);
        
    }

    protected override void PlayerClicked()
    {
        base.PlayerClicked();
        
        if (isRequireMagnifier)
        {
            if (!GameManager.Instance.IsUsingMagnifier)
            {
                return;
            }
        }
        
        if (!objectiveData.IsComplete)
        {
            objectiveData.Complete();
            
            if(completeSound)
                audioSource.PlayOneShot(completeSound);
            OnComplete?.Invoke();
        }
    }
}
