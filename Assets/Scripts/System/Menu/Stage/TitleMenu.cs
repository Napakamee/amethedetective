using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleMenu : MonoBehaviour
{
    private Canvas canvas;
    
    void Update()
    {
        if (Input.anyKey)
        {
            canvas = gameObject.GetComponent<Canvas>();
            GameMenuManager.OpenMenu(Menu.MAINMENU, canvas);
            gameObject.SetActive(false);
        }
    }
}
