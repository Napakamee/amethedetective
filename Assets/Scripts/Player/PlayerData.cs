using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Data/PlayerData")]
public class PlayerData : ScriptableObject
{
    [SerializeField] private bool unlockedMagnifier;
    [SerializeField] private bool unlockedGun;
    [SerializeField] private StageData[] stageDatas;

    public bool UnlockedMagnifier
    {
        get => unlockedMagnifier;
        set => unlockedMagnifier = value;
    }

    public bool UnlockedGun
    {
        get => unlockedGun;
        set => unlockedGun = value;
    }

    public StageData[] StageDatas => stageDatas;

    public void UpdateStageStateComplete(int _index)
    {
        stageDatas[_index].IsComplete = true;
    }

    public void UnlockState(int _index)
    {
        
        stageDatas[_index].IsUnlock = true;
        Debug.Log("Unlocked " + _index);
    }

    private void ResetProgress()
    {
        foreach (var _data in stageDatas)
        {
            _data.ResetData();
        }
    }
}
