using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class RangeEnemy : EnemyBase
{
    [SerializeField] private float attackCd = 1;
    [SerializeField] private float bulletSpeed = 7f;
    
    
    protected override void UpdateFound()
    {
        base.UpdateFound();
        if (canAttack)
        {
            Attack();
        }
    }

    private void Attack()
    {
        canAttack = false;
        if (PoolManager.Instance.bulletPool.Count > 1)
        {
            Bullet.SpawnBullet(transform.position, bulletSpeed, facingDirection, BulletType.Enemy);
        }
        else
        {
            Bullet.CreateBullet(transform.position, bulletSpeed, facingDirection, BulletType.Enemy);
        }
        StartCoroutine(AbilityCD());
    }
    
    private IEnumerator AbilityCD()
    {
        yield return new WaitForSeconds(attackCd);
        canAttack = true;
    }
}
