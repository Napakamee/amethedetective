// GENERATED AUTOMATICALLY FROM 'Assets/InputSystem/PlayerInputActions.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @PlayerInputActions : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @PlayerInputActions()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerInputActions"",
    ""maps"": [
        {
            ""name"": ""Movement"",
            ""id"": ""e1dbf6ef-3645-48e3-bd45-90f9d0ca95c4"",
            ""actions"": [
                {
                    ""name"": ""Move"",
                    ""type"": ""PassThrough"",
                    ""id"": ""86393a32-3198-413f-a8e7-bcc157f386f6"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""aeb42a20-0cf3-4f96-9c4f-f2a47bfc2562"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Down"",
                    ""type"": ""Button"",
                    ""id"": ""3b26b16c-0c62-4d88-9a52-4d74820ac494"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ClimbUp"",
                    ""type"": ""Button"",
                    ""id"": ""edfbefbc-b3d6-4444-bedb-392f9e91cc58"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ClimbDown"",
                    ""type"": ""Button"",
                    ""id"": ""170e57af-74d0-43e8-83f0-4546ad2caf3f"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""WASD"",
                    ""id"": ""5941cb6c-3e5e-40fb-8ecb-12d2b2741526"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""3fc7c805-0004-4407-9730-639b323b01aa"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""17838e32-77e0-4e2f-a44c-2af854a00344"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""9dfafd57-8f44-4e49-8c89-4a4887aac04e"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""09d7078f-c578-489f-9c2c-b068aa42f5bc"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Move"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""aed856c6-c6df-4249-9cc4-befa947e7a5d"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""98c2aa94-0ee9-4178-9f4b-b368354c9143"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Down"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""398e5f87-8c03-46cc-9975-844f33bc7d3b"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ClimbUp"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5ce6e4f4-4282-4caa-8971-02cfb4a7eebe"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ClimbDown"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""ShortcutKeys"",
            ""id"": ""60d06c16-1e48-4abb-8778-82308252b7a5"",
            ""actions"": [
                {
                    ""name"": ""Esc"",
                    ""type"": ""Button"",
                    ""id"": ""722555e9-6f34-412f-8110-7402ab83a196"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ChangeChar"",
                    ""type"": ""PassThrough"",
                    ""id"": ""1acb602d-f74e-4352-96c4-ba88da3ab1f3"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""ToggleMagnifier"",
                    ""type"": ""Button"",
                    ""id"": ""c2028158-15f0-4c6c-93c7-08fed96a1da5"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""cb50adc7-976d-4f82-b3a7-ae4e4b294aae"",
                    ""path"": ""<Keyboard>/escape"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Esc"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""1D Axis"",
                    ""id"": ""1f3412c5-96c3-43e6-96ef-2e693068dad5"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ChangeChar"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""1bd55939-2141-4b28-a9d9-e951f6580f57"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ChangeChar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""b601b1a7-2e74-40de-9715-ca5987fdc2e9"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ChangeChar"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""b73d7a7a-d4f6-4df9-a2b0-dd0fbc9a00d6"",
                    ""path"": ""<Keyboard>/f"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ToggleMagnifier"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""MouseInput"",
            ""id"": ""d8a1c5bd-2e42-47f3-a582-20f40d14edff"",
            ""actions"": [
                {
                    ""name"": ""LeftClick"",
                    ""type"": ""Button"",
                    ""id"": ""2349619a-fc24-4198-8044-615fb7e71904"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""RightClick"",
                    ""type"": ""Button"",
                    ""id"": ""c588e388-8d20-4c43-9c7a-871de58f1268"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MousePosition"",
                    ""type"": ""Value"",
                    ""id"": ""dae23d02-013e-4454-9804-c447cc73684b"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""b9c93772-b365-498c-9de0-d6995dcc6e07"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""LeftClick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f7ac5ff1-6a53-49fb-807c-e35acf5189b8"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""RightClick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4ae41059-dc55-4ae6-988d-f38aaaab3b4e"",
                    ""path"": ""<Mouse>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MousePosition"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Movement
        m_Movement = asset.FindActionMap("Movement", throwIfNotFound: true);
        m_Movement_Move = m_Movement.FindAction("Move", throwIfNotFound: true);
        m_Movement_Jump = m_Movement.FindAction("Jump", throwIfNotFound: true);
        m_Movement_Down = m_Movement.FindAction("Down", throwIfNotFound: true);
        m_Movement_ClimbUp = m_Movement.FindAction("ClimbUp", throwIfNotFound: true);
        m_Movement_ClimbDown = m_Movement.FindAction("ClimbDown", throwIfNotFound: true);
        // ShortcutKeys
        m_ShortcutKeys = asset.FindActionMap("ShortcutKeys", throwIfNotFound: true);
        m_ShortcutKeys_Esc = m_ShortcutKeys.FindAction("Esc", throwIfNotFound: true);
        m_ShortcutKeys_ChangeChar = m_ShortcutKeys.FindAction("ChangeChar", throwIfNotFound: true);
        m_ShortcutKeys_ToggleMagnifier = m_ShortcutKeys.FindAction("ToggleMagnifier", throwIfNotFound: true);
        // MouseInput
        m_MouseInput = asset.FindActionMap("MouseInput", throwIfNotFound: true);
        m_MouseInput_LeftClick = m_MouseInput.FindAction("LeftClick", throwIfNotFound: true);
        m_MouseInput_RightClick = m_MouseInput.FindAction("RightClick", throwIfNotFound: true);
        m_MouseInput_MousePosition = m_MouseInput.FindAction("MousePosition", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Movement
    private readonly InputActionMap m_Movement;
    private IMovementActions m_MovementActionsCallbackInterface;
    private readonly InputAction m_Movement_Move;
    private readonly InputAction m_Movement_Jump;
    private readonly InputAction m_Movement_Down;
    private readonly InputAction m_Movement_ClimbUp;
    private readonly InputAction m_Movement_ClimbDown;
    public struct MovementActions
    {
        private @PlayerInputActions m_Wrapper;
        public MovementActions(@PlayerInputActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @Move => m_Wrapper.m_Movement_Move;
        public InputAction @Jump => m_Wrapper.m_Movement_Jump;
        public InputAction @Down => m_Wrapper.m_Movement_Down;
        public InputAction @ClimbUp => m_Wrapper.m_Movement_ClimbUp;
        public InputAction @ClimbDown => m_Wrapper.m_Movement_ClimbDown;
        public InputActionMap Get() { return m_Wrapper.m_Movement; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MovementActions set) { return set.Get(); }
        public void SetCallbacks(IMovementActions instance)
        {
            if (m_Wrapper.m_MovementActionsCallbackInterface != null)
            {
                @Move.started -= m_Wrapper.m_MovementActionsCallbackInterface.OnMove;
                @Move.performed -= m_Wrapper.m_MovementActionsCallbackInterface.OnMove;
                @Move.canceled -= m_Wrapper.m_MovementActionsCallbackInterface.OnMove;
                @Jump.started -= m_Wrapper.m_MovementActionsCallbackInterface.OnJump;
                @Jump.performed -= m_Wrapper.m_MovementActionsCallbackInterface.OnJump;
                @Jump.canceled -= m_Wrapper.m_MovementActionsCallbackInterface.OnJump;
                @Down.started -= m_Wrapper.m_MovementActionsCallbackInterface.OnDown;
                @Down.performed -= m_Wrapper.m_MovementActionsCallbackInterface.OnDown;
                @Down.canceled -= m_Wrapper.m_MovementActionsCallbackInterface.OnDown;
                @ClimbUp.started -= m_Wrapper.m_MovementActionsCallbackInterface.OnClimbUp;
                @ClimbUp.performed -= m_Wrapper.m_MovementActionsCallbackInterface.OnClimbUp;
                @ClimbUp.canceled -= m_Wrapper.m_MovementActionsCallbackInterface.OnClimbUp;
                @ClimbDown.started -= m_Wrapper.m_MovementActionsCallbackInterface.OnClimbDown;
                @ClimbDown.performed -= m_Wrapper.m_MovementActionsCallbackInterface.OnClimbDown;
                @ClimbDown.canceled -= m_Wrapper.m_MovementActionsCallbackInterface.OnClimbDown;
            }
            m_Wrapper.m_MovementActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Move.started += instance.OnMove;
                @Move.performed += instance.OnMove;
                @Move.canceled += instance.OnMove;
                @Jump.started += instance.OnJump;
                @Jump.performed += instance.OnJump;
                @Jump.canceled += instance.OnJump;
                @Down.started += instance.OnDown;
                @Down.performed += instance.OnDown;
                @Down.canceled += instance.OnDown;
                @ClimbUp.started += instance.OnClimbUp;
                @ClimbUp.performed += instance.OnClimbUp;
                @ClimbUp.canceled += instance.OnClimbUp;
                @ClimbDown.started += instance.OnClimbDown;
                @ClimbDown.performed += instance.OnClimbDown;
                @ClimbDown.canceled += instance.OnClimbDown;
            }
        }
    }
    public MovementActions @Movement => new MovementActions(this);

    // ShortcutKeys
    private readonly InputActionMap m_ShortcutKeys;
    private IShortcutKeysActions m_ShortcutKeysActionsCallbackInterface;
    private readonly InputAction m_ShortcutKeys_Esc;
    private readonly InputAction m_ShortcutKeys_ChangeChar;
    private readonly InputAction m_ShortcutKeys_ToggleMagnifier;
    public struct ShortcutKeysActions
    {
        private @PlayerInputActions m_Wrapper;
        public ShortcutKeysActions(@PlayerInputActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @Esc => m_Wrapper.m_ShortcutKeys_Esc;
        public InputAction @ChangeChar => m_Wrapper.m_ShortcutKeys_ChangeChar;
        public InputAction @ToggleMagnifier => m_Wrapper.m_ShortcutKeys_ToggleMagnifier;
        public InputActionMap Get() { return m_Wrapper.m_ShortcutKeys; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(ShortcutKeysActions set) { return set.Get(); }
        public void SetCallbacks(IShortcutKeysActions instance)
        {
            if (m_Wrapper.m_ShortcutKeysActionsCallbackInterface != null)
            {
                @Esc.started -= m_Wrapper.m_ShortcutKeysActionsCallbackInterface.OnEsc;
                @Esc.performed -= m_Wrapper.m_ShortcutKeysActionsCallbackInterface.OnEsc;
                @Esc.canceled -= m_Wrapper.m_ShortcutKeysActionsCallbackInterface.OnEsc;
                @ChangeChar.started -= m_Wrapper.m_ShortcutKeysActionsCallbackInterface.OnChangeChar;
                @ChangeChar.performed -= m_Wrapper.m_ShortcutKeysActionsCallbackInterface.OnChangeChar;
                @ChangeChar.canceled -= m_Wrapper.m_ShortcutKeysActionsCallbackInterface.OnChangeChar;
                @ToggleMagnifier.started -= m_Wrapper.m_ShortcutKeysActionsCallbackInterface.OnToggleMagnifier;
                @ToggleMagnifier.performed -= m_Wrapper.m_ShortcutKeysActionsCallbackInterface.OnToggleMagnifier;
                @ToggleMagnifier.canceled -= m_Wrapper.m_ShortcutKeysActionsCallbackInterface.OnToggleMagnifier;
            }
            m_Wrapper.m_ShortcutKeysActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Esc.started += instance.OnEsc;
                @Esc.performed += instance.OnEsc;
                @Esc.canceled += instance.OnEsc;
                @ChangeChar.started += instance.OnChangeChar;
                @ChangeChar.performed += instance.OnChangeChar;
                @ChangeChar.canceled += instance.OnChangeChar;
                @ToggleMagnifier.started += instance.OnToggleMagnifier;
                @ToggleMagnifier.performed += instance.OnToggleMagnifier;
                @ToggleMagnifier.canceled += instance.OnToggleMagnifier;
            }
        }
    }
    public ShortcutKeysActions @ShortcutKeys => new ShortcutKeysActions(this);

    // MouseInput
    private readonly InputActionMap m_MouseInput;
    private IMouseInputActions m_MouseInputActionsCallbackInterface;
    private readonly InputAction m_MouseInput_LeftClick;
    private readonly InputAction m_MouseInput_RightClick;
    private readonly InputAction m_MouseInput_MousePosition;
    public struct MouseInputActions
    {
        private @PlayerInputActions m_Wrapper;
        public MouseInputActions(@PlayerInputActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @LeftClick => m_Wrapper.m_MouseInput_LeftClick;
        public InputAction @RightClick => m_Wrapper.m_MouseInput_RightClick;
        public InputAction @MousePosition => m_Wrapper.m_MouseInput_MousePosition;
        public InputActionMap Get() { return m_Wrapper.m_MouseInput; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MouseInputActions set) { return set.Get(); }
        public void SetCallbacks(IMouseInputActions instance)
        {
            if (m_Wrapper.m_MouseInputActionsCallbackInterface != null)
            {
                @LeftClick.started -= m_Wrapper.m_MouseInputActionsCallbackInterface.OnLeftClick;
                @LeftClick.performed -= m_Wrapper.m_MouseInputActionsCallbackInterface.OnLeftClick;
                @LeftClick.canceled -= m_Wrapper.m_MouseInputActionsCallbackInterface.OnLeftClick;
                @RightClick.started -= m_Wrapper.m_MouseInputActionsCallbackInterface.OnRightClick;
                @RightClick.performed -= m_Wrapper.m_MouseInputActionsCallbackInterface.OnRightClick;
                @RightClick.canceled -= m_Wrapper.m_MouseInputActionsCallbackInterface.OnRightClick;
                @MousePosition.started -= m_Wrapper.m_MouseInputActionsCallbackInterface.OnMousePosition;
                @MousePosition.performed -= m_Wrapper.m_MouseInputActionsCallbackInterface.OnMousePosition;
                @MousePosition.canceled -= m_Wrapper.m_MouseInputActionsCallbackInterface.OnMousePosition;
            }
            m_Wrapper.m_MouseInputActionsCallbackInterface = instance;
            if (instance != null)
            {
                @LeftClick.started += instance.OnLeftClick;
                @LeftClick.performed += instance.OnLeftClick;
                @LeftClick.canceled += instance.OnLeftClick;
                @RightClick.started += instance.OnRightClick;
                @RightClick.performed += instance.OnRightClick;
                @RightClick.canceled += instance.OnRightClick;
                @MousePosition.started += instance.OnMousePosition;
                @MousePosition.performed += instance.OnMousePosition;
                @MousePosition.canceled += instance.OnMousePosition;
            }
        }
    }
    public MouseInputActions @MouseInput => new MouseInputActions(this);
    public interface IMovementActions
    {
        void OnMove(InputAction.CallbackContext context);
        void OnJump(InputAction.CallbackContext context);
        void OnDown(InputAction.CallbackContext context);
        void OnClimbUp(InputAction.CallbackContext context);
        void OnClimbDown(InputAction.CallbackContext context);
    }
    public interface IShortcutKeysActions
    {
        void OnEsc(InputAction.CallbackContext context);
        void OnChangeChar(InputAction.CallbackContext context);
        void OnToggleMagnifier(InputAction.CallbackContext context);
    }
    public interface IMouseInputActions
    {
        void OnLeftClick(InputAction.CallbackContext context);
        void OnRightClick(InputAction.CallbackContext context);
        void OnMousePosition(InputAction.CallbackContext context);
    }
}
