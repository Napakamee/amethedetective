using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Module.Interact
{
    public class InteractToggle : InteractableObject
    {
        [SerializeField][ReadOnly] private bool isOn = false;
        [Header("Object")]
        [SerializeField] private GameObject on;
        [SerializeField] private GameObject off;
        [SerializeField] private GameObject content;

        public bool IsOn => isOn;

        public override void Init()
        {
            base.Init();
            isOn = true;
            //content.transform.localPosition = new Vector3(0, 0, -0.5f);
            ToggleObject();
        }

        public override void OnClick(int _inputID)
        {
            base.OnClick(_inputID);
            // if (playerInRange)
            // {
            //     ToggleObject();
            // }
        }

        protected override void PlayerClicked()
        {
            base.PlayerClicked();
            ToggleObject();
        }

        private void ToggleObject()
        {
            isOn = !isOn;
            on.SetActive(isOn);
            off.SetActive(!isOn);
            if (content)
            {
                content.SetActive(isOn);
            }
        }
    }
}
