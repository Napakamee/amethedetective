using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;


public enum DialogMode { Auto, Click }

namespace Dialog
{
    public class DialogBase : DialogRunner
    {
        #region Inspector

        
        [SerializeField][ReadOnly] protected DialogMode dialogMode;

        [Header("Optional")] 
        [SerializeField] protected DialogData dialogData;

        [SerializeField] protected SpeakerDatabase speakerDatabase;

        #endregion

        #region Public

        public float ChangeLineSpeed { get => changeLineSpeed; set => changeLineSpeed = value; }
        public DialogMode DialogMode { get => dialogMode; set => dialogMode = value; }
        

        #endregion
        
        #region Private/Protected

        /// <summary>
        /// Set to false when don't want to hide container.
        /// </summary>
        protected bool hideObjectWhenDialogEnd = true;
        
        protected float changeLineSpeed = 1f;
        
        protected Coroutine dialogCoroutine;

        protected bool isLineFinished = false;
        
        protected AudioSource audioSource;

        #endregion
        
        #region Data
        
        protected Dictionary<string, SpeakerData> speakerDataDict = new Dictionary<string, SpeakerData>();

        protected Dictionary<string, SpriteRenderer> speakerSpriteRenderer = new Dictionary<string, SpriteRenderer>();
        
        private Dictionary<string, TagCommand> tagCommands = new Dictionary<string, TagCommand>();

        #endregion
        
        #region Setup

        public virtual void Init()
        {
            
            foreach (var _button in DialogUI.OptionButtons) {
                _button.gameObject.SetActive (false);
            }
            
            if (DialogUI.DialogContainer != null && hideObjectWhenDialogEnd)
                DialogUI.DialogContainer.SetActive(false);
            else if (!DialogUI.DialogContainer)
                Debug.LogWarning("Dialog container is null.");
            
            
            LinkCallback();
            DialogUI.OnFindTag += TryRunTagCommand;

            audioSource = GetComponent<AudioSource>();
            if (!audioSource)
            {
                audioSource = gameObject.AddComponent<AudioSource>();
            }
        }

        private void LinkCallback()
        {
            DialogUI.OnDialogStart += DialogStart;
            DialogUI.OnDialogEnd += DialogEnd;
            DialogUI.OnLineStart += LineStart;
            DialogUI.OnLineFinishDisplaying += LineFinished;
            DialogUI.OnLineUpdate += LineUpdate;
            DialogUI.OnLineEnd += LineEnd;
            DialogUI.OnOptionsStart += OptionsStart;
            DialogUI.OnOptionsEnd += OptionsEnd;
            DialogUI.OnCommand += Command;
            OnDialogComplete += Complete;
        }

        /// <summary>
        /// Set custom command that can be use in yarn editor.
        /// </summary>
        protected virtual void SetCommand()
        {
            AddFunction("random",1 
                , _parameters => GetRandomValue((int)_parameters[0].AsNumber));
            
            
            AddCommandHandler("play_sound", PlaySound);
            //AddCommandHandler("set_sprite", SetSprite);

            //Test
            AddTagCommand("emote", EmoteCommand);
        }

        /// <summary>
        /// Add sprite data of speaker.
        /// </summary>
        /// <param name="_data"></param>
        /// <param name="_renderer"></param>
        public virtual void AddSpeaker(SpeakerData _data, SpriteRenderer _renderer = null)
        {
            if (_data == null || _renderer == null)
                return;
            
            if (speakerDataDict.ContainsKey(_data.ID))
            {
                Debug.LogWarningFormat("Attempting to add {0} into speaker sprite database, but it already exists!", _data.ID);
                return;
            }
            
            speakerSpriteRenderer.Add(_data.ID, _renderer);
            speakerDataDict.Add(_data.ID, _data);
        }
        

        private void EmoteCommand(string[] _param)
        {
            foreach (var _s in _param)
            {
                Debug.LogFormat("{0} is {1}",DialogUI.CurrentSpeaker, _s);
            }
            
        }

        #region Tags

        private void AddTagCommand(string _commandName, TagCommand _command)
        {
            if (tagCommands.ContainsKey(_commandName)) {
                Debug.LogError($"Cannot add a tag command for {_commandName}: one already exists");
                return;
            }
            tagCommands.Add(_commandName, _command);
        }

        private void TryRunTagCommand(string _command, string[] _param)
        {
            if (tagCommands.ContainsKey(_command))
            {
                tagCommands[_command].Invoke(_param);
            }
            else
            {
                Debug.LogWarningFormat("Can't find {0} in tagCommands dictionary, check the spelling or command doesn't exit.", _command);
            }
        }

        #endregion

        #endregion

        #region Command/Utility

        protected virtual void PlaySound(string[] _param)
        {
            string _soundName = _param[0];
            audioSource.PlayOneShot(FetchAsset<AudioClip>(_soundName));
            //Debug.LogFormat("Play {0}", _soundName);
        }

        protected virtual void SetSprite(string[] _param)
        {
            string _speaker = _param[0];
            string _spriteName = _param[1].ToLower();
            speakerSpriteRenderer[_speaker].sprite = GetSprite(_speaker, _spriteName);
            
        }
        
        protected virtual Sprite GetSprite(string _speaker, string _spriteName)
        {
            return null;  //speakerSpriteDatabase[_speaker].AnimationList[_spriteName];
        }
        
        /// <summary>
        /// Random function that return int and can be use in yarn editor. 
        /// </summary>
        /// <example>random("3")</example>
        /// <param name="_max">Max number to random.</param>
        /// <returns></returns>
        private int GetRandomValue(int _max)
        {
            return Random.Range(0, _max);
        }
        
        //From VNManager.cs in YarnSpinner 2.0
        // utility function to find an asset, whether it's in \Resources\
        // or manually loaded via an array
        protected virtual T FetchAsset<T>( string assetName ) where T : UnityEngine.Object {
            // first, check to see if it's a manully loaded asset, with
            // manual array checks... it's messy but I can't think of a
            // better way to do this
            if ( typeof(T) == typeof(Sprite) ) {
                foreach ( var spr in dialogData.Sprites ) {
                    if (spr.name == assetName) {
                        return spr as T;
                    }
                }
            } else if ( typeof(T) == typeof(AudioClip) ) {
                foreach ( var ac in dialogData.Audio ) {
                    if ( ac.name == assetName ) {
                        return ac as T;
                    }
                }
            }else if ( typeof(T) == typeof(SpeakerData) ) {
                foreach ( var _spk in speakerDatabase.SpeakerDatas ) {
                    if (_spk.ID == assetName) {
                        return _spk as T;
                    }
                }
            }

            // by default, we load all Resources assets into the asset
            // arrays already, but if you don't want that, then uncomment
            // this, etc. if ( useResourcesFolders ) {var newAsset =
            // Resources.Load<T>(assetName); if ( newAsset != null )
            // {return newAsset;
            //  }
            // }

            Debug.LogErrorFormat(this, "Can't find asset [{0}]... maybe it is misspelled, or isn't imported as {1}?", assetName, typeof(T).ToString() );
            return null; // didn't find any matching asset
        }

        #endregion

        #region DialogState

        public virtual void StartDialog(string _startNode)
        {
            if (!this.gameObject.activeSelf)
            {
                gameObject.SetActive(true);
            }
            
            if (DialogUI.DialogContainer != null && hideObjectWhenDialogEnd)
                DialogUI.DialogContainer.SetActive(true);
            
            //Stop current dialog before running new one.
            Stop();
            StartDialogue(_startNode);
        }

        public virtual void DialogStart() { }
        
        public virtual void DialogEnd()
        {
            if (dialogCoroutine != null) StopCoroutine(dialogCoroutine);
            // Hide the dialogue interface.
            if (DialogUI.DialogContainer != null && hideObjectWhenDialogEnd)
                DialogUI.DialogContainer.SetActive(false);
        }

        public virtual void LineStart()
        {
            isLineFinished = false;
        }
        
        public virtual void LineFinished()
        {
            if (dialogCoroutine != null) StopCoroutine(dialogCoroutine);

            //If Auto mode change new line.
            if (dialogMode == DialogMode.Auto)
            {
                dialogCoroutine = StartCoroutine(IELineFinished());
            }

            isLineFinished = true;
        }

        /// <summary>
        /// Update text.
        /// </summary>
        public virtual void LineUpdate(string _str)
        {
            DialogUI.DialogText.text = _str;
        }

        public virtual void LineEnd() { }
        
        public virtual void OptionsStart() { }
        
        public virtual void OptionsEnd() { }

        public virtual void Command(string _str) { }
        
        public virtual void Complete() { }
        
        private IEnumerator IELineFinished()
        {
            yield return new WaitForSeconds(changeLineSpeed);
            DialogUI.MarkLineComplete();
            //Debug.LogFormat("Line finished!");
        }

        #endregion

        public delegate void TagCommand(string[] _param);
    }
}
