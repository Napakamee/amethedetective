using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SpriteDict : UnitySerializedDictionary<string, Sprite>{}

[CreateAssetMenu(menuName = "Dialog/SpeakerData")]
public class SpeakerData : ScriptableObject
{
    #region Inspector
    [SerializeField] private string id;
    [SerializeField] private List<string> alias = new List<string>();
    [SerializeField] private Color color = Color.white;
    [SerializeField] private SpriteDict sprites;
    
    #endregion

    public const string NATURAL = "natural";

    public string ID => id;

    public Color Color => color;

    public List<string> Alias => alias;

    public SpriteDict Sprites => sprites;
}