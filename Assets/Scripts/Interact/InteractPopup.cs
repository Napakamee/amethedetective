using System.Collections;
using System.Collections.Generic;
using Module.Interact;
using UnityEngine;

public class InteractPopup : InteractableObject
{
    [SerializeField] private PopupObject popupObject;
    public override void Init()
    {
        base.Init();
        if (popupObject)
        {
            popupObject = Instantiate(popupObject, GameObject.Find("PopupCanvas").transform);
            popupObject.Init();
        }
        else
        {
            Debug.LogError($"popupObject is missing in {gameObject.name}");
        }
    }

    public override void OnClick(int _inputID)
    {
        base.OnClick(_inputID);
        if (playerInRange && !GameManager.Instance.GameIsPaused && !GameManager.Instance.IsInteracting)
        {
            popupObject.Open();
        }
    }

    protected override void PlayerClicked()
    {
        base.PlayerClicked();
        
    }
}
