using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Audio;

[RequireComponent(typeof(AudioSource))]
public class SoundManager : Singleton<SoundManager>
{
    [SerializeField] private AudioSource sfxSource;
    [SerializeField] private AudioSource bgmSource;
    
    public void PlaySFX(AudioClip _audio)
    {
        if (!sfxSource)
        {
            var _mixer = Resources.Load("Mixer") as AudioMixer;
            sfxSource = gameObject.AddComponent<AudioSource>();
            sfxSource.outputAudioMixerGroup = _mixer.FindMatchingGroups("SFX")[0];
        }
        sfxSource.PlayOneShot(_audio);
        
    }

    public void PlayBGM(AudioClip _audio)
    {
        if (!bgmSource)
        {
            var _mixer = Resources.Load("Mixer") as AudioMixer;
            bgmSource = gameObject.AddComponent<AudioSource>();
            bgmSource.outputAudioMixerGroup = _mixer.FindMatchingGroups("BGM")[0];
            bgmSource.loop = true;
        }

        bgmSource.clip = _audio;

        bgmSource.Play();
    }
}
