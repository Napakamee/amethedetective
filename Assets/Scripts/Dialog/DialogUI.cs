using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Yarn;
using Yarn.Unity;

namespace Dialog
{
    public class DialogUI : DialogueUIBehaviour
    {
        public const float DefaultTextSpeed = 0.025f;

        #region Inspector
        [Header("DialogUI")]
        [SerializeField] protected TextMeshProUGUI dialogText;
        [SerializeField] protected GameObject dialogContainer;
        [SerializeField] protected List<Button> optionButtons = new List<Button>();
        [SerializeField][ReadOnly] protected string currentSpeaker;
        
        #endregion

        #region Private

        private float textSpeed = 0.025f;
        private List<string> speakerNames = new List<string>();
        private List<char> charactersToType;
        private bool userRequestedNextLine = false;
        private bool waitingForOptionSelection = false;
        
        private Action<int> currentOptionSelectionHandler;

        #endregion
        
        #region Callback

        /// <summary>
        /// Called when dialogue starts.
        /// </summary>
        /// <remarks>
        /// Use this to do things like change your camera, disable gameplay UI,
        /// and other tasks you might need to do when dialogue begins.
        /// </remarks>
        public Action OnDialogStart;
        
        /// <summary>
        /// Called when the dialogue ends.
        /// </summary>
        /// <remarks>
        /// Use this event to disable any dialogue-related UI and gameplay
        /// elements, and enable any non-dialogue UI and gameplay elements.
        /// </remarks>
        public Action OnDialogEnd;
        
        /// <summary>
        /// Called when a <see cref="Line"/> has been delivered.
        /// </summary>
        /// <remarks>
        /// This method is called before <see cref="OnLineUpdate"/> is
        /// called. Use this event to prepare the scene to deliver a line.
        /// </remarks>
        public Action OnLineStart;
        
        /// <summary>
        /// Called when a line has finished being delivered.
        /// </summary>
        /// <seealso cref="OnLineUpdate"/>
        /// <seealso cref="Yarn.Unity.DialogueUI.MarkLineComplete"/>
        public Action OnLineFinishDisplaying;
        
        /// <summary>
        /// Called when the visible part of the line's localised text changes.
        /// </summary>
        /// <seealso cref="Yarn.Unity.DialogueUI.onLineUpdate"/>
        public Action<string> OnLineUpdate;
        
        /// <summary>
        /// Called when a line has finished displaying, and should be removed from
        /// the screen.
        /// </summary>
        public Action OnLineEnd;
        
        /// <summary>
        /// Called when an <see cref="OptionSet"/> has been displayed to the user.
        /// </summary>
        /// <seealso cref="Yarn.Unity.DialogueUI.onOptionsStart"/>
        public Action OnOptionsStart;
        
        /// <summary>
        /// Called when an option has been selected, and the <see
        /// cref="Yarn.Unity.DialogueUI.optionButtons"/> should be hidden.
        /// </summary>
        /// <seealso cref="Yarn.Unity.DialogueUI.onOptionsEnd"/>
        public Action OnOptionsEnd;
        
        /// <summary>
        /// Called when a <see cref="Command"/> is received.
        /// </summary>
        /// <seealso cref="Yarn.Unity.DialogueUI.onCommand"/>
        public Action<string> OnCommand;
        
        public Action<string> OnFindNewSpeaker;

        public Action<string, string[]> OnFindTag;
        
        public Action<string> OnSpeakerChanged;

        #endregion

        #region Public

        public float TextSpeed { get => textSpeed; set => textSpeed = value; }

        public List<Button> OptionButtons { get => optionButtons; set => optionButtons = value; }

        public GameObject DialogContainer { get => dialogContainer; set => dialogContainer = value; }

        public TextMeshProUGUI DialogText => dialogText;

        public string CurrentSpeaker
        {
            get => currentSpeaker;
            set => currentSpeaker = value;
        }

        #endregion
        
        #region DialogueUI

        /// Runs a line.
        /// <inheritdoc/>
        public override Dialogue.HandlerExecutionType RunLine (Yarn.Line _line, ILineLocalisationProvider _localisationProvider, System.Action _onLineComplete)
        {
            // Start displaying the line; it will call onComplete later
            // which will tell the dialogue to continue
            StartCoroutine(DoRunLine(_line, _localisationProvider, _onLineComplete));
            return Dialogue.HandlerExecutionType.PauseExecution;
        }
        
        
        //TODO: Change this to TMP.maxVisibleCharacters
        /// Show a line of dialogue, gradually        
        private IEnumerator DoRunLine(Yarn.Line _line, ILineLocalisationProvider _localisationProvider, System.Action _onComplete) {
            OnLineStart?.Invoke();

            userRequestedNextLine = false;
            
            // The final text we'll be showing for this line.
            string _text = _localisationProvider.GetLocalisedTextForLine(_line);

            if (_text == null) {
                Debug.LogWarning($"Line {_line.ID} doesn't have any localised text.");
                _text = _line.ID;
            }
            
            //Get speaker name.
            TagReader.SplitSpeakerName(_text,  out string _speaker, out _text);
            
            if (_speaker != null)
            {
                currentSpeaker = _speaker;
            }
            else
            {
                currentSpeaker = "";
            }
            OnSpeakerChanged?.Invoke(currentSpeaker);
            
            TagReader.SplitTextTags(_text, out List<TagInfo> _tags, out _text);
            ProcessText(_text);
            dialogText.text = _text;

            if (textSpeed > 0.0f) {
                
                //Show text one by one by increasing maxVisibleCharacters.
                for (int _numPrintedChars = 0; _numPrintedChars < this.charactersToType.Count; ++_numPrintedChars)
                {
                    foreach (var _tag in _tags)
                    {
                        //Current pos has tag
                        if (_numPrintedChars == _tag.StartPos)
                        {
                            OnFindTag?.Invoke(_tag.Name, _tag.AttributeValue);
                            //Debug.LogFormat("Find {0}", _tag.Name);
                        }
                    }
                    
                    dialogText.maxVisibleCharacters = _numPrintedChars + 1;
                    dialogText.ForceMeshUpdate();

                    var _printedChar = this.charactersToType[_numPrintedChars];
                    //this.OnCharacterPrinted(printedChar.ToString());

                    yield return new WaitForSeconds(textSpeed);
                    
                }
            } 
            
            if(userRequestedNextLine)
            {
                // Display the entire line immediately if textSpeed <= 0
                dialogText.maxVisibleCharacters = 9999;
                dialogText.ForceMeshUpdate();
                OnLineUpdate?.Invoke(_text);
            }

            // We're now waiting for the player to move on to the next line
            userRequestedNextLine = false;

            // Indicate to the rest of the game that the line has finished being delivered
            OnLineFinishDisplaying?.Invoke();

            while (userRequestedNextLine == false) {
                yield return null;
            }

            // Avoid skipping lines if textSpeed == 0
            yield return new WaitForEndOfFrame();

            // Hide the text and prompt
            OnLineEnd?.Invoke();

            _onComplete();

        }

        private void ProcessText(string _text)
        {
            charactersToType = new List<char>();
            
            int _printedCharCount = 0;
            foreach (var _char in _text)
            {
                _printedCharCount++;
                charactersToType.Add(_char);
            }
        }
        
        /// Runs a set of options.
        /// <inheritdoc/>
        public override void RunOptions (Yarn.OptionSet _optionSet, ILineLocalisationProvider _localisationProvider, System.Action<int> _onOptionSelected) {
            StartCoroutine(DoRunOptions(_optionSet, _localisationProvider, _onOptionSelected));
        }
        
        /// Show a list of options, and wait for the player to make a
        /// selection.
        private  IEnumerator DoRunOptions (Yarn.OptionSet _optionsCollection, ILineLocalisationProvider _localisationProvider, System.Action<int> _selectOption)
        {
            // Do a little bit of safety checking
            if (_optionsCollection.Options.Length > optionButtons.Count) {
                Debug.LogWarning("There are more options to present than there are" +
                                 "buttons to present them in. This will cause problems.");
            }

            // Display each option in a button, and make it visible
            int i = 0;

            waitingForOptionSelection = true;

            currentOptionSelectionHandler = _selectOption;
            
            foreach (var _optionString in _optionsCollection.Options) {
                optionButtons [i].gameObject.SetActive (true);

                // When the button is selected, tell the dialogue about it
                optionButtons [i].onClick.RemoveAllListeners();
                optionButtons [i].onClick.AddListener(() => SelectOption(_optionString.ID));

                var _optionText = _localisationProvider.GetLocalisedTextForLine(_optionString.Line);

                if (_optionText == null) {
                    Debug.LogWarning($"Option {_optionString.Line.ID} doesn't have any localised text");
                    _optionText = _optionString.Line.ID;
                }

                var _unityText = optionButtons [i].GetComponentInChildren<Text> ();
                if (_unityText != null) {
                    _unityText.text = _optionText;
                }

                var _textMeshProText = optionButtons [i].GetComponentInChildren<TMPro.TMP_Text> ();
                if (_textMeshProText != null) {
                    _textMeshProText.text = _optionText;
                }

                i++;
            }

            OnOptionsStart?.Invoke();

            // Wait until the chooser has been used and then removed 
            while (waitingForOptionSelection) {
                yield return null;
            }

            
            // Hide all the buttons
            foreach (var _button in optionButtons) {
                _button.gameObject.SetActive (false);
            }

            OnOptionsEnd?.Invoke();

        }
        
        /// Runs a command.
        /// <inheritdoc/>
        public override Dialogue.HandlerExecutionType RunCommand (Yarn.Command _command, System.Action _onCommandComplete) {
            // Dispatch this command via the 'On Command' handler.
            OnCommand?.Invoke(_command.Text);

            // Signal to the DialogueRunner that it should continue
            // executing. (This implementation of RunCommand always signals
            // that execution should continue, and never calls
            // onCommandComplete.)
            return Dialogue.HandlerExecutionType.ContinueExecution;
        }
        
        /// Called when the dialogue system has started running.
        /// <inheritdoc/>
        public override void DialogueStarted ()
        {
            // Enable the dialogue controls.
            // if (dialogueContainer != null)
            //     dialogueContainer.SetActive(true);

            OnDialogStart?.Invoke();            
        }

        /// Called when the dialogue system has finished running.
        /// <inheritdoc/>
        public override void DialogueComplete ()
        {
            OnDialogEnd?.Invoke();

            // // Hide the dialogue interface.
            // if (dialogueContainer != null)
            //     dialogueContainer.SetActive(false);
            
        }

        /// <summary>
        /// Signals that the user has finished with a line, or wishes to
        /// skip to the end of the current line.
        /// </summary>
        /// <remarks>
        /// This method is generally called by a "continue" button, and
        /// causes the DialogueUI to signal the <see
        /// cref="DialogueRunner"/> to proceed to the next piece of
        /// content.
        ///
        /// If this method is called before the line has finished appearing
        /// (that is, before <see cref="onLineFinishDisplaying"/> is
        /// called), the DialogueUI immediately displays the entire line
        /// (via the <see cref="onLineUpdate"/> method), and then calls
        /// <see cref="onLineFinishDisplaying"/>.
        /// </remarks>
        public void MarkLineComplete() {
            userRequestedNextLine = true;
        }

        /// <summary>
        /// Signals that the user has selected an option.
        /// </summary>
        /// <remarks>
        /// This method is called by the <see cref="Button"/>s in the <see
        /// cref="optionButtons"/> list when clicked.
        ///
        /// If you prefer, you can also call this method directly.
        /// </remarks>
        /// <param name="_optionID">The <see cref="OptionSet.Option.ID"/> of
        /// the <see cref="OptionSet.Option"/> that was selected.</param>
        public void SelectOption(int _optionID) {
            if (waitingForOptionSelection == false) {
                Debug.LogWarning("An option was selected, but the dialogue UI was not expecting it.");
                return;
            }
            waitingForOptionSelection = false;
            currentOptionSelectionHandler?.Invoke(_optionID);
        }
        

        #endregion
    }
}
