using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/StageData")]
public class StageData: ScriptableObject
{
    [SerializeField] private string name;
    [SerializeField] private bool forcedUnlock;
    [SerializeField] private bool isUnlock;
    [SerializeField] private bool isComplete;

    public bool IsComplete { get => isComplete; set => isComplete = value; }
    public string Name => name;
    public bool IsUnlock { get => isUnlock; set => isUnlock = value; }

    public bool ForcedUnlock => forcedUnlock;

    public void ResetData()
    {
        isUnlock = forcedUnlock;
        isComplete = false;
    }
}
