using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private List<EnemyBase> enemiesToSpawn;


    private void Start()
    {
        foreach (var _enemy in enemiesToSpawn)
        {
            _enemy.gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D _other)
    {
        if (_other.CompareTag("Player"))
        {
            foreach (var _enemy in enemiesToSpawn)
            {
                _enemy.gameObject.SetActive(true);
            }
            Destroy(gameObject);
        }
    }
}
