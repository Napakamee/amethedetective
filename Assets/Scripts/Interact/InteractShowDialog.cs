using System;
using System.Collections;
using System.Collections.Generic;
using Dialog;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Module.Interact
{
    public class InteractShowDialog : InteractableObject
    {
        
        [SerializeField] 
        protected string nodeToStart;

        private SceneLoader sceneLoader;
        private void Awake()
        {
            sceneLoader = FindObjectOfType<SceneLoader>();
        }

        public override void OnClick(int _inputID)
        {
            base.OnClick(_inputID);
            if (playerInRange)
            {
                sceneLoader.StoryUI.StartDialog(nodeToStart);
            }
        }
    }
}

