using System;
using System.Collections;
using System.Collections.Generic;
using Dialog;
using Module.Interact;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public Action<CharacterEnum> OnUnlockedChar;
    [SerializeField] private AudioClip changeSceneSfx;
    [SerializeField] private AudioClip bgm;
    [SerializeField] private int stageIndex;
    [SerializeField] private bool isResetPlayerData = false;
    [SerializeField] private Camera UICamera;
    [SerializeField] private GameObject popupCanvas;
    [SerializeField] private DialogStoryUI dialogStoryUIPrefab;
    [SerializeField] private Animator transitionAnimator;
    [SerializeField] private AnimationClip inOutClip;
    [SerializeField] private string startCutSceneNode;
    [SerializeField] private string endCutSceneNode;

    [SerializeField] private TextMeshProUGUI objCount;
    [Space]
    [SerializeField] private List<CharacterEnum> playableChar = new List<CharacterEnum>();
    [SerializeField] private List<ObjectiveData> extraObjectives = new List<ObjectiveData>();
    
    private DialogStoryUI dialogStoryUI;
    private bool isInit = false;
    private int currentObj = 0;

    public GameObject PopupCanvas { get => popupCanvas; set => popupCanvas = value; }
    
    public DialogStoryUI StoryUI { get => dialogStoryUI; set => dialogStoryUI = value; }

    public List<ObjectiveData> ExtraObjectives => extraObjectives;

    protected  void Awake()
    {
        transitionAnimator.Play("In");
        SoundManager.Instance.PlayBGM(bgm);
        
        GameManager.Instance.IsResetPlayerData = isResetPlayerData;
        GameManager.Instance.PlayableChar = playableChar;
        GameManager.Instance.Init();

        Init();
        
        var _objects = FindObjectsOfType<InteractableObject>(true);
        foreach (var _object in _objects)
        {
            _object.Init();
        }
        
        currentObj = 0;
        objCount.SetText(currentObj + "/5");
        foreach (var _objective in GameManager.Instance.InteractObjectives)
        {
            _objective.OnComplete += delegate
            {
                currentObj++;
                objCount.SetText(currentObj + "/5");
            };
        }
        
    }

    private void Init()
    {
        if (isInit) return;
        PoolManager.Instance.Init();
        
        StoryUI = Instantiate(dialogStoryUIPrefab);
        StoryUI.GetComponent<Canvas>().worldCamera = UICamera;
        StoryUI.Init();

        if (!string.IsNullOrWhiteSpace(startCutSceneNode) && GameManager.Instance.IsPlayDialog)
        {
            dialogStoryUI.StartDialog(startCutSceneNode);
        }

        foreach (var _stageData in GameManager.Instance.Data.StageDatas)
        {
            _stageData.IsComplete = false;
        }

        popupCanvas = Instantiate(popupCanvas);
        popupCanvas.GetComponent<Canvas>().worldCamera = UICamera;
        popupCanvas.name = "PopupCanvas";
    }

    public void StageComplete()
    {
        GameManager.Instance.UpdateStageState(stageIndex);
        
        if (!string.IsNullOrWhiteSpace(endCutSceneNode))
        {
            dialogStoryUI.StartDialog(endCutSceneNode);
        }
    }

    public void UnlockChar(int _character)
    {
        OnUnlockedChar?.Invoke((CharacterEnum) _character);
    }
    
    public void UnlockGun()
    {
        GameManager.Instance.UnlockGun();
    }
    
    public void UnlockMagnifier()
    {
        GameManager.Instance.UnlockMagnifier();
    }

    public void NextScene(string _scene)
    {
        if (GameManager.Instance.IsComplete)
        {
            GameManager.Instance.IsPlayDialog = true;
            StartCoroutine(IEChangeScene(_scene));
        }
    }

    public void ChangeScene(string _scene)
    {
        PoolManager.Instance.Clear();
        GameManager.Instance.IsPlayDialog = true;
        SceneManager.LoadScene(_scene);
    }

    public void GameOver()
    {
        GameManager.Instance.IsGameOver = true;
        GameManager.Instance.IsPlayDialog = false;
        StartCoroutine(IEGameOver(SceneManager.GetActiveScene().name));
    }

    public void QuitGame()
    {
        Application.Quit();
    }
    
    public IEnumerator IEGameOver(string _scene, Action _onComplete = null)
    {
        var _anim = inOutClip.length;
        transitionAnimator.Play("Out");
        yield return new WaitForSeconds(_anim);
        PoolManager.Instance.Clear();
        SceneManager.LoadScene(_scene);
    }

    public IEnumerator IEChangeScene(string _scene, Action _onComplete = null)
    {
        SoundManager.Instance.PlaySFX(changeSceneSfx);
        var _anim = inOutClip.length;
        transitionAnimator.Play("Out");
        yield return new WaitForSeconds(_anim);
        PoolManager.Instance.Clear();
        GameManager.Instance.NextScene(_scene);
    }
    
}
