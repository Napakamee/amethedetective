using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Character/CharacterData")]
public class CharacterData : ScriptableObject
{
    [SerializeField] private string name;
    [SerializeField] private CharacterEnum character;
    [SerializeField] private Sprite characterImage;
    public string Name => name;
    public CharacterEnum Character => character;
    public Sprite CharacterImage => characterImage;
}
