using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PlayerSoundController : MonoBehaviour
{
    [SerializeField][ReadOnly] private AudioSource audioSource;

    [SerializeField] [FoldoutGroup("Audios")] private AudioClip damageSound;
    
    [SerializeField] [FoldoutGroup("Audios")] private AudioClip guraAttackSound;
    [SerializeField] [FoldoutGroup("Audios")] private AudioClip watsonAttackSound;
    [SerializeField] [FoldoutGroup("Audios")] private AudioClip inaAttackSound;
    
    private AudioClip walkSound;
    private AudioClip interactSound;
    public AudioSource Audio => audioSource;

    public AudioClip GuraAttackSound => guraAttackSound;

    public AudioClip WalkSound => walkSound;

    public AudioClip InteractSound => interactSound;

    public AudioClip WatsonAttackSound => watsonAttackSound;

    public AudioClip InaAttackSound => inaAttackSound;

    public AudioClip DamageSound => damageSound;

    private void OnValidate()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    public void PlaySound(AudioClip _audio)
    {
        if (_audio)
        {
            audioSource.PlayOneShot(_audio);
        }
    }
}
