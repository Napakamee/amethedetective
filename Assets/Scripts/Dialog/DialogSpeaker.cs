using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogSpeaker : MonoBehaviour
{
    [SerializeField] private SpeakerData speakerData;
    private RectTransform rectTransform;
    private Image image;

    public RectTransform RectTransform
    {
        get => rectTransform;
        set => rectTransform = value;
    }

    public SpeakerData SpeakerData
    {
        get => speakerData;
        set => speakerData = value;
    }

    public Image Image
    {
        get => image;
        set => image = value;
    }

    public void Init(SpeakerData _data, Image _image, RectTransform _rect)
    {
        SpeakerData = _data;
        image = _image;
        rectTransform = _rect;
        
    }

    public void SetImage(string _imageName)
    {
        if (speakerData)
        {
            if (speakerData.Sprites.ContainsKey(_imageName))
            {
                this.image.sprite = speakerData.Sprites[_imageName];
                rectTransform.sizeDelta = image.sprite.rect.size;
                UIUtilities.SetPreferredSize(rectTransform);
            }
            else
            {
                Debug.LogErrorFormat("{0} doesn't exit in {1}'s speaker data.", _imageName, speakerData.ID);
            }
        }
        
    }
    
}
