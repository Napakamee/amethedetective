using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public static class GameMenuManager
{
    public static bool IsInitilised { get; private set; }
    public static Canvas titleMenu, mainMenu, stageSelectMenu, settingMenu, creditMenu;

    
    
    public static void Init()
    {
        GameObject canvas = GameObject.Find("Canvases");
        titleMenu = canvas.transform.Find("Canvas_Title").GetComponent<Canvas>();
        mainMenu = canvas.transform.Find("Canvas_Menu").GetComponent<Canvas>();
        stageSelectMenu = canvas.transform.Find("Canvas_StageSelect").GetComponent<Canvas>();
        settingMenu = canvas.transform.Find("Canvas_Setting").GetComponent<Canvas>();
        creditMenu = canvas.transform.Find("Canvas_Credit").GetComponent<Canvas>();
        
        titleMenu.gameObject.SetActive(true);
        mainMenu.gameObject.SetActive(false);
        stageSelectMenu.gameObject.SetActive(false);
        settingMenu.gameObject.SetActive(false);
        creditMenu.gameObject.SetActive(false);
    }

    public static void OpenMenu(Menu menu, Canvas callingCanvas)
    {

        switch (menu)
        {
            case Menu.TITLEMENU:
                titleMenu.gameObject.SetActive(true);
                break;
            case Menu.MAINMENU:
                mainMenu.gameObject.SetActive(true);
                break;
            case Menu.STAGESELECTMENU:
                stageSelectMenu.gameObject.SetActive(true);
                break;
            case Menu.SETTINGMENU:
                settingMenu.gameObject.SetActive(true);
                break;
            case Menu.CREDITMENU:
                creditMenu.gameObject.SetActive(true);
                break;
        }
        callingCanvas.gameObject.SetActive(false);
    }
}
