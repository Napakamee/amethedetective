using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CurrentCharUI : MonoBehaviour
{
    [SerializeField] private List<CharacterData> characterDatas;
    [SerializeField] private Image charImage;
    void Start()
    {
        var _player = FindObjectOfType<Player>();
        _player.OnChangeChar += UpdateChar;
    }

    private void UpdateChar(CharacterEnum _newChar)
    {
        foreach (var _character in characterDatas)
        {
            if (_character.Character == _newChar)
            {
                charImage.sprite = _character.CharacterImage;
            }
        }
    }
}
