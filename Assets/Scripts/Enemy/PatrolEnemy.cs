using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;

public class PatrolEnemy : EnemyBase
{
    [SerializeField] private List<Transform> wayPoints = new List<Transform>();
    [SerializeField] private float waitTimeBetweenPoint = 0f;
    [SerializeField] protected float stopRange = 0.2f;
    public bool cyclic = true;
    [Range(0,2)]
    public float easeAmount = 1;
    private int waypointIndex = 0;
    private bool isMoving;
    private float nextMoveTime;
    private float percentBetweenWaypoints;

    private List<Vector3> WaypointPos => wayPoints.Select(_wayPoint => new Vector3(_wayPoint.localPosition.x, transform.position.y, transform.position.z)).ToList();

    protected override void Start()
    {
        base.Start();
        if (wayPoints.Count <= 1)
        {
            Debug.LogErrorFormat("Need 2 waypoints in {0}", name);
        }
    }

    protected override void UpdateMoving()
    {
        FlipRotate();
        
        var _velocity = CalculatePlatformMovement();
        _velocity = new Vector2(_velocity.x, rb.velocity.y);
        
        facingDirection = _velocity.x <= 0 ? -1 : 1;

        rb.velocity = _velocity;
    }

    protected override void UpdateFound()
    {
        base.UpdateFound();
        FlipRotate();
        FollowTargetWithoutRotation(player.transform, stopRange, moveSpeed);
    }
    
    void FollowTargetWithoutRotation(Transform _target, float _distanceToStop, float _speed)
    {
        if(Vector3.Distance(transform.position, _target.position) > _distanceToStop)
        {
            facingDirection = (int) (_target.position.x - transform.position.x);
            var _direction = new Vector3(facingDirection, 0, 0);
            
            rb.velocity = _direction.normalized * _speed;
        }
    }

    //From: https://github.com/SebLague/2DPlatformer-Tutorial/blob/master/Platformer%20E08/Assets/Scripts/PlatformController.cs
    Vector3 CalculatePlatformMovement() {

        if (Time.time < nextMoveTime) {
            return Vector3.zero;
        }

        waypointIndex %= WaypointPos.Count;
        int _toWaypointIndex = (waypointIndex + 1) % WaypointPos.Count;
        float _distanceBetweenWaypoints = Vector3.Distance (WaypointPos[waypointIndex], WaypointPos[_toWaypointIndex]);
        percentBetweenWaypoints += Time.deltaTime * moveSpeed/_distanceBetweenWaypoints;
        percentBetweenWaypoints = Mathf.Clamp01 (percentBetweenWaypoints);
        float _easedPercentBetweenWaypoints = Ease (percentBetweenWaypoints);

        Vector3 _newPos = Vector3.Lerp (WaypointPos [waypointIndex], WaypointPos [_toWaypointIndex], _easedPercentBetweenWaypoints);

        if (percentBetweenWaypoints >= 1) {
            percentBetweenWaypoints = 0;
            waypointIndex ++;

            if (!cyclic) {
                if (waypointIndex >= WaypointPos.Count-1) {
                    waypointIndex = 0;
                    WaypointPos.Reverse();
                }
            }
            nextMoveTime = Time.time + waitTimeBetweenPoint;
        }

        return _newPos - transform.position;
    }
    
    float Ease(float _x) {
        float _a = easeAmount + 1;
        return Mathf.Pow(_x,_a) / (Mathf.Pow(_x,_a) + Mathf.Pow(1-_x,_a));
    }

    private void WaitForNextMove(Action _onComplete = null)
    {
        StartCoroutine(IEWaitForNextMove(_onComplete));
    }

    private IEnumerator IEWaitForNextMove(Action _onComplete)
    {
        yield return new WaitForSeconds(waitTimeBetweenPoint);
        _onComplete?.Invoke();
    }

    protected override void OnDrawGizmos()
    {
        base.OnDrawGizmos();
        Gizmos.color = Color.red;
        foreach (var _wayPoint in wayPoints)
        {
            if (_wayPoint)
            {
                Gizmos.DrawSphere(_wayPoint.localPosition, 0.2f);
            }
        }
    }
}
