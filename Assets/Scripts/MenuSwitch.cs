using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuSwitch : MonoBehaviour
{
    [SerializeField] private Animator stateCamAnimator;
    [SerializeField] private AudioClip bgm;

    private void Start()
    {
        SoundManager.Instance.PlayBGM(bgm);
    }

    public void BtnMenu()
    {
        stateCamAnimator.Play("mainmenu");
    }

    public void BtnStageSelect()
    {
        stateCamAnimator.Play("stageselect");
    }

    public void BtnOptions()
    {
        stateCamAnimator.Play("option");
    }

    public void BtnCredits()
    {
        stateCamAnimator.Play("credit");
    }
}
