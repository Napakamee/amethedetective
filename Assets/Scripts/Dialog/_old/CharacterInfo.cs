using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/CharacterInfo")]
public class CharacterInfo : ScriptableObject
{
    [SerializeField] private string displayName;
    

    [SerializeField] private Sprite sprite;
    
    public string Name => displayName;
    
    public Sprite Sprite => sprite;
}