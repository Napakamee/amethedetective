using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public enum BulletType{Enemy, Player}

[RequireComponent(typeof(Rigidbody2D))]
public class Bullet : MonoBehaviour
{
    
    private const string BulletPrefabPath = "Prefabs/Bullet";
    private Rigidbody2D rb;
    [SerializeField] private GameObject playerBullet;
    [SerializeField] private GameObject enemyBullet;
    [SerializeField][ReadOnly] private BulletType bulletType;
    public static void SpawnBullet(Vector3 _spawnPos, float _speed, int _direction, BulletType _type)
    {
       var _bullet = PoolManager.Instance.bulletPool.Dequeue();
       _bullet.tag = _type == BulletType.Enemy ? "Enemy" : "AttackBox";
       _bullet.gameObject.SetActive(true);
       _bullet.bulletType = _type;
       _bullet.transform.position = _spawnPos;
       _bullet.rb = _bullet.GetComponent<Rigidbody2D>();
       _bullet.rb.gravityScale = 0f;
       _bullet.rb.velocity = Vector2.right * _direction * _speed;
       
       var _transformLocalScale = _bullet.transform.localScale;
       _transformLocalScale.x = _direction;
       _bullet.transform.localScale = _transformLocalScale;
       
       if (_type == BulletType.Enemy)
       {
           _bullet.enemyBullet.SetActive(true);
           _bullet.playerBullet.SetActive(false);
       }
       else
       {
           _bullet.enemyBullet.SetActive(false);
           _bullet.playerBullet.SetActive(true);
       }
    }

    public static void CreateBullet(Vector3 _spawnPos, float _speed, int _direction, BulletType _type)
    {
        var _bullet = Instantiate(Resources.Load<Bullet>(BulletPrefabPath));
        _bullet.tag = _type == BulletType.Enemy ? "Enemy" : "AttackBox";
        _bullet.bulletType = _type;
        _bullet.transform.position = _spawnPos;
        _bullet.rb = _bullet.GetComponent<Rigidbody2D>();
        _bullet.rb.gravityScale = 0f;
        _bullet.rb.velocity = Vector2.right * _direction * _speed;
        
        var _transformLocalScale = _bullet.transform.localScale;
        _transformLocalScale.x = _direction;
        _bullet.transform.localScale = _transformLocalScale;
        
        if (_type == BulletType.Enemy)
        {
            _bullet.enemyBullet.SetActive(true);
            _bullet.playerBullet.SetActive(false);
        }
        else
        {
            _bullet.enemyBullet.SetActive(false);
            _bullet.playerBullet.SetActive(true);
        }
        
    }

    private void Update()
    {
        if (transform.position.x < CameraExtension.GetMinX() - 5)
        {
            DeSpawn();
        }
        else if (transform.position.x > CameraExtension.GetMaxX() + 5)
        {
            DeSpawn();
        }
    }

    private void DeSpawn()
    {
        rb.velocity = Vector2.zero;
        gameObject.SetActive(false);
        PoolManager.Instance.bulletPool.Enqueue(this);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        
        switch (bulletType)
        {
            case BulletType.Enemy:
            {
                if (other.CompareTag("Player"))
                {
                    other.GetComponent<IDamageable>()?.ApplyDamage(1, transform.position.x);
                    DeSpawn();
                }
                break;
            }
            case BulletType.Player:
                if (!other.CompareTag("Player"))
                {
                    if (other.TryGetComponent<IDamageable>(out var _damageable))
                    {
                        _damageable?.ApplyDamage(2, transform.position.x);
                        DeSpawn();
                    }
                    
                }
                break;
        }
    }
}
