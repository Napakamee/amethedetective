using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/Dialog")]
public class DialogTextData : ScriptableObject
{
    [SerializeField]
    [Searchable]
    [DetailedInfoBox("Click to view how to use...",
        "<b>Dialog Feature:</b> An extra feature of dialog.\n" +
             "<b>-Side Char:</b> Non speaker, use when need 2 people talking.\n" +
             "<b>-Cutscene:</b> Show cutscene, enable in every dialog that want to have but only need to assign sprite once." +
             "To change cutscene just simply put new cutscene in [Cutscene Sprite]. To end cutscene uncheck it.\n" +
             "\n" +
             "<b>Main Character:</b> Character that speaking (have name show in dialog).\n" +
             "\n" +
             "<b>Sprite Position:</b> Position of sprite to display. Don't set same position for both main and side character."
        )]
    [ListDrawerSettings( ShowIndexLabels = false, NumberOfItemsPerPage = 5)]
    private List<DialogText> dialog;
    
    public List<DialogText> Dialog => dialog;
    
}

public enum SpritePosition
{
    Left, Middle, Right
}

[Flags]
public enum DialogFeature
{
    None,
    SideChar = 1 << 1,
    Cutscene = 1 << 2,
    Both = SideChar | Cutscene
}

[Serializable]
public struct DialogText
{

    [FoldoutGroup("$Name")]
    [HideLabel][Header("Dialog Features")]
    [EnumToggleButtons]
    public DialogFeature dialogFeature;
    [Space]
    
    [FoldoutGroup("$Name")]
    [Tooltip("Main speaker.")]
    public CharacterInfo mainCharacter;
    
    [FoldoutGroup("$Name")]
    [EnumToggleButtons]
    [HideLabel][Header("Sprite Position")]
    public SpritePosition mainCharPosition;
    [Title("")]
    
    [FoldoutGroup("$Name")]
    [ShowIf("@dialogFeature == DialogFeature.SideChar || dialogFeature == DialogFeature.Both")]
    public CharacterInfo sideCharacter;
    
    [FoldoutGroup("$Name")]
    [EnumToggleButtons]
    [HideLabel][Header("Sprite Position")]
    [ShowIf("@dialogFeature == DialogFeature.SideChar || dialogFeature == DialogFeature.Both")]
    public SpritePosition sideCharPosition;

    [FoldoutGroup("$Name")]
    [HideLabel][Header("Cutscene Sprite")]
    [ShowIf("@dialogFeature == DialogFeature.Cutscene || dialogFeature == DialogFeature.Both")]
    public Sprite cutsceneSprite;
    
    [FoldoutGroup("$Name")]
    [HideLabel][Title("Dialog")]
    [MultiLineProperty(5)]
    public string text;
    
    private string Name => mainCharacter ? mainCharacter.Name : "Main Character is Null!";
    
}
