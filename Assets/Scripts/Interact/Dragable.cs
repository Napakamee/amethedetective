using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Rigidbody2D))]
public class Dragable : MonoBehaviour, IDragHandler, IEndDragHandler, IBeginDragHandler
{
    [SerializeField] private bool canDrag = true;
    [SerializeField] private float dragSpeed = 5f;
    
    [SerializeField][ReadOnly] private Rigidbody2D rb;


    private InputManager inputManager;
    private bool isDragging;
    public bool CanDrag { get => canDrag; set => canDrag = value; }

    #region OnValidate

    private void OnValidate()
    {
        SetupRigidbody();
    }

    private void SetupRigidbody()
    {
        if (!rb)
        {
            if (!TryGetComponent(out rb))
            {
                rb = gameObject.AddComponent<Rigidbody2D>();
            }
        }

        rb.SetUpRigidBody();
    }

    #endregion
    
    private void Start()
    {
        var _player = FindObjectOfType<PlayerBase>();
        inputManager = FindObjectOfType<InputManager>();
        _player.OnMove += Dragging;
    }

    private void Update()
    {
        if (!isDragging)
        {
            rb.isKinematic = rb.velocity == Vector2.zero;
        }
    }

    private void FixedUpdate()
    {
        UpdateDrag();
    }

    private void UpdateDrag()
    {
        Dragging();
    }

    private void StartDrag()
    {
        if(!canDrag) return;
        isDragging = true;
        rb.isKinematic = false;
        rb.gravityScale = 0f;
    }

    private void Dragging()
    {
        if(!canDrag) return;
        if(!isDragging) return;
        Vector3 _mousePos = inputManager.PlayerInput.MouseInput.MousePosition.ReadValue<Vector2>();

        _mousePos.z = 10f;
        _mousePos = Camera.main.ScreenToWorldPoint(_mousePos);
        _mousePos.z = 0f;

        rb.velocity = (_mousePos - rb.transform.position) * dragSpeed;
        
    }

    private void EndDrag()
    {
        isDragging = false;
        rb.gravityScale = 1f;
    }


    #region CallbackHandler

    public void OnDrag(PointerEventData _eventData)
    {
        //Dragging();
    }

    public void OnEndDrag(PointerEventData _eventData)
    {
        EndDrag();
    }

    public void OnBeginDrag(PointerEventData _eventData)
    {
        if(GameManager.Instance.CurrentChar != CharacterEnum.Inanis) return;
        
        if (_eventData.button == PointerEventData.InputButton.Right)
        {
            StartDrag();
        }
    }

    #endregion
    
}
