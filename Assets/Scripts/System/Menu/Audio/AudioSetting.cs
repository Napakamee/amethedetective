using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class AudioSetting : MonoBehaviour
{
    [SerializeField] private AudioMixer mixer;
    [SerializeField] private Slider masterSlider;
    [SerializeField] private Slider bgmSlider;
    [SerializeField] private Slider sfxSlider;
    
    private void Start()
    {
        masterSlider.value = PlayerPrefs.GetFloat("MasterVolume", 0.75f);
        bgmSlider.value = PlayerPrefs.GetFloat("BGMVolume", 0.75f);
        sfxSlider.value = PlayerPrefs.GetFloat("SFXVolume", 0.75f);
        
        ChangeMasterVol(masterSlider.value);
        ChangeMusicVol(bgmSlider.value);
        ChangeSFXVol(bgmSlider.value);
        
        masterSlider.onValueChanged.AddListener(ChangeMasterVol);
        bgmSlider.onValueChanged.AddListener(ChangeMusicVol);
        sfxSlider.onValueChanged.AddListener(ChangeSFXVol);
    }
    
    public void ChangeMasterVol(float _value)
    {
        float _val = masterSlider.value;
        mixer.SetFloat("MasterVol", Mathf.Log10(_val) * 20);
        PlayerPrefs.SetFloat("MasterVolume", _val);
    }
    
    public void ChangeMusicVol(float _value)
    {
        float _val = bgmSlider.value;
        mixer.SetFloat("BGMVol", Mathf.Log10(_val) * 20);
        PlayerPrefs.SetFloat("BGMVolume", _val);
    }
    
    public void ChangeSFXVol(float _value)
    {
        float _val = sfxSlider.value;
        mixer.SetFloat("SFXVol", Mathf.Log10(_val) * 20);
        PlayerPrefs.SetFloat("SFXVolume", _val);
    }
    
}
