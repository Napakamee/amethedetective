using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    private Canvas canvas;
    [SerializeField] private Animator stateCamAnimator;
    
    public void OnClick_Start()
    {
        SceneManager.LoadScene("Act1_Ame_House");
    }
    
    public void OnClick_StageSelect()
    {
        canvas = gameObject.GetComponent<Canvas>();
        stateCamAnimator.Play("stageselect");
        GameMenuManager.OpenMenu(Menu.STAGESELECTMENU, canvas);
    }

    public void OnClick_Setting()
    {
        canvas = gameObject.GetComponent<Canvas>();
        stateCamAnimator.Play("option");
        GameMenuManager.OpenMenu(Menu.SETTINGMENU, canvas);
    }
    
    public void OnClick_Credit()
    {
        canvas = gameObject.GetComponent<Canvas>();
        
        stateCamAnimator.Play("credit");
        GameMenuManager.OpenMenu(Menu.CREDITMENU, canvas);
    }

    public void OnClick_Quit()
    {
        Application.Quit();
    }
}
