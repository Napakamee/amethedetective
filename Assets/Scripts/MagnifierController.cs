using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MagnifierController : MonoBehaviour
{
    [SerializeField] private Button toggleButton;
    [SerializeField] private Magnifier magnifier;
    

    private void Start()
    {
        toggleButton.onClick.AddListener(ToggleMagnifier);
        toggleButton.interactable = GameManager.Instance.IsUnlockMagnifier;

        var _input = FindObjectOfType<InputManager>();
        _input.OnToggleMagnifier += _context => ToggleMagnifier();
    }

    public void Unlock()
    {
        toggleButton.interactable = GameManager.Instance.IsUnlockMagnifier;
    }

    private void ToggleMagnifier()
    {
        magnifier.ToggleActive();
    }
}
