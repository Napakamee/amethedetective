using System;
using System.Linq;
using DG.Tweening;
using UnityEngine;
 
public static class Helper
{
    public static GameObject FindInChildren(this GameObject _go, string _name)
    {
        return (from x in _go.GetComponentsInChildren<Transform>()
            where x.gameObject.name == _name
            select x.gameObject).First();
    }

    public static void SetUpRigidBody(this Rigidbody _rb)
    {
        _rb.collisionDetectionMode = CollisionDetectionMode.Continuous;
        _rb.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionZ;
    }
    
    public static void SetUpRigidBody(this Rigidbody2D _rb)
    {
        _rb.collisionDetectionMode = CollisionDetectionMode2D.Continuous;
        _rb.constraints = RigidbodyConstraints2D.FreezeRotation;
    }

    public static void MoveToPosition(this Rigidbody _rb, Vector3 _target, float _duration, Action _onComplete = null)
    {
        Tweener _tweener = _rb.DOMoveX(_target.x, _duration).SetEase(Ease.Linear);
        _tweener.onComplete = () => _onComplete?.Invoke();
    }
    public static void MoveToPosition(this Rigidbody2D _rb, Vector3 _target, float _duration, Action _onComplete = null)
    {
        Tweener _tweener = _rb.DOMoveX(_target.x, _duration).SetEase(Ease.Linear);
        _tweener.onComplete = () => _onComplete?.Invoke();
    }
}