using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;


    public class TagInfo
    {
        private int startPos;
        private int length;
        private int tagLength;
        private string name;
        private string[] attributeValue;
        private TagInfo endTag;

        #region Public

        public int StartPos { get => startPos; set => startPos = value; }
        public int Length { get => length; set => length = value; }
        public int TagLength { get => tagLength; set => tagLength = value; }
        public string Name { get => name; set => name = value; }
        public string[] AttributeValue { get => attributeValue; set => attributeValue = value; }
        public TagInfo EndTag { get => endTag; set => endTag = value; }

        #endregion
    }

    public class ParsedText
    {
        private string text;
        private TagInfo[] tags;

        #region Public

        public string Text { get => text; set => text = value; }
        public TagInfo[] Tags { get => tags; set => tags = value; }

        #endregion
    }

        public static class TagReader
    {
        public static void SplitSpeakerName(string _input, out string _speaker, out string _text)
        {
            //Get speaker name.
            string[] _splitText = _input.Split(new[] { ": " }, StringSplitOptions.None);
            if (_splitText.Length >= 2)
            {
                _speaker = _splitText[0];
                _text = _splitText[1];
                return;
            }

            _speaker = null;
            _text = _input;
        }

        //Get tag with <@tag> format, return plain text and tags.
        public static void SplitTextTags(string _input, out List<TagInfo> _tags, out string _text)
        {
            //@"(?<=\<@)(.*?)(?=\>)"
            Regex _regex = new Regex(@"\<@(\w+)(\s+=\s*([^\>]+))?\>");
            _tags = new List<TagInfo>();
            var _charTags = _regex.Matches(_input);
            var _posWithoutTags = 0;

            foreach (Match _match in _charTags)
            {

                _tags.Add(new TagInfo()
                {
                    Name = _match.Groups[1].Value,
                    StartPos = _match.Groups[0].Index - _posWithoutTags,
                    TagLength = _match.Groups[0].Length,
                    AttributeValue = SplitStringBySpace(_match.Groups[3].Value)
                });
                
                //Add tag length 
                _posWithoutTags += _match.Groups[0].Length;
                
            }

            _text = _regex.Replace(_input, string.Empty);
        }


        //Edited from https://stackoverflow.com/questions/42608146/parsing-tags-in-string/42612491
        public static ParsedText ParseString(string _input)
        {
            List<TagInfo> _tagList = new List<TagInfo>();
            Regex _regex = new Regex(@"\<(\w+)(\s*=\s*([^\>]+))?\>|\<(\/\w+)\>|\<(\w+)(\s+(\w+)\s*=\s*([^\>]+))?\>");
            
            return new ParsedText()
            {
                Text = _regex.Replace(_input, string.Empty),
                Tags = _tagList.ToArray()
            };
        }

        public static string RemoveYarnTextAssetPrefix(string _input)
        {
            string[] _strings = _input.Split(',');
            if (_input.Split('"').Length > 1)
            {
                _strings = _input.Split('"');
            }
            return _strings[1];
        }
        
        public static string[] SplitStringBySpace(string _input)
        {
            return _input.Split(' ');
        }
    }

