using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Dialog/SpeakerDatabase", fileName = "[SpeakerDatabase]")]
public class SpeakerDatabase : ScriptableObject
{
    [SerializeField] private List<SpeakerData> speakerDatas = new List<SpeakerData>();

    public List<SpeakerData> SpeakerDatas
    {
        get => speakerDatas;
        set => speakerDatas = value;
    }
}