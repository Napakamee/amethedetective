using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magnifier : MonoBehaviour
{
    
    private bool isActive = false;

    public bool IsActive => isActive;

    private InputManager inputManager;

    private void Start()
    {
        gameObject.SetActive(false);
        inputManager = FindObjectOfType<InputManager>();
    }

    public void ToggleActive()
    {
        isActive = !isActive;
        gameObject.SetActive(isActive);
        GameManager.Instance.IsUsingMagnifier = isActive;
    }

    private void Update()
    {
        
        if (isActive)
        {
            Vector3 _mousePos = inputManager.PlayerInput.MouseInput.MousePosition.ReadValue<Vector2>();

            _mousePos.z = 0f;
            _mousePos = Camera.main.ScreenToWorldPoint(_mousePos);
            _mousePos.z = -2.5f;

            gameObject.transform.position = _mousePos;
        }
    }
}
