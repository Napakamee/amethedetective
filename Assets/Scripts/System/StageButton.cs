using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageButton : MonoBehaviour
{
    [SerializeField] private Button button;
    [SerializeField] private Image lockImage;
    [SerializeField] private StageData stageData;

    public Button Button
    {
        get => button;
        set => button = value;
    }

    public Image LockImage
    {
        get => lockImage;
        set => lockImage = value;
    }

    private void Start()
    {
        button.interactable = stageData.IsUnlock;
        lockImage.gameObject.SetActive(!stageData.IsUnlock);
    }
}
