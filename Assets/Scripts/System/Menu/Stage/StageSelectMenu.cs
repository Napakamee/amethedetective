using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StageSelectMenu : MonoBehaviour
{
    private Canvas canvas;
    [SerializeField] private StageData stage3;
    [SerializeField] private MainMenu mainMenu;
    [SerializeField] private TitleMenu titleMenu;
    private void Start()
    {
        GameMenuManager.Init();
        canvas = gameObject.GetComponent<Canvas>();
        
        if (stage3.IsComplete)
        {
            titleMenu.gameObject.SetActive(false);
            mainMenu.OnClick_StageSelect();
        }
    }

    public void OnClick_Back()
    {
        canvas = gameObject.GetComponent<Canvas>();
        GameMenuManager.OpenMenu(Menu.MAINMENU, canvas);
    }

    public void OnClick_Stage(string scene)
    {
        SceneManager.LoadScene(scene);
    }
}
