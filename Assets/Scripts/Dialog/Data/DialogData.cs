using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Dialog/DialogData")]
public class DialogData : ScriptableObject
{
    [SerializeField] private List<Sprite> sprites;
    [SerializeField] private List<AudioClip> audio;

    public List<Sprite> Sprites => sprites;

    public List<AudioClip> Audio => audio;
}
