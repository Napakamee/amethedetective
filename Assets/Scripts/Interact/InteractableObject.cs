using System;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.EventSystems;

namespace Module.Interact
{
    [RequireComponent(typeof(AudioSource))]
    public class InteractableObject : MonoBehaviour, IClickable, IPointerClickHandler
    {
        
        public Action OnPlayerClicked;
        [SerializeField] protected bool isUI = false;
        [SerializeField] private bool isDebug = false;

        [SerializeField] protected bool isRequireMagnifier = false;
        [SerializeField][FoldoutGroup("Basic Module")] protected AudioClip interactSound;

        [SerializeField][FoldoutGroup("Basic Module")] protected bool specificChar;

        [SerializeField] [FoldoutGroup("Basic Module")]
        [ShowIf("specificChar")]protected CharacterEnum interactableChar = CharacterEnum.Inanis;
        [SerializeField][FoldoutGroup("Basic Module")] protected bool shake = true;
        [SerializeField][FoldoutGroup("Basic Module")] [ReadOnly] private GameObject rangeCheckColliderPrefab;
        [SerializeField][FoldoutGroup("Basic Module")] [ReadOnly] private RangeCheckCollider rangeCheckCollider;
        
        protected AudioSource audioSource;
        
        private SpriteRenderer spriteRenderer;
        private Tweener shakeTween;
        [SerializeField] [ReadOnly] protected bool playerInRange;

        protected bool isInit = false;

        private Color startColor;

        [Button]
        public void SetUpObject()
        {
            if (!isUI)
            {
                if (!rangeCheckColliderPrefab)
                {
                    rangeCheckColliderPrefab = Resources.Load("Prefabs/InteractCollider") as GameObject;
                }
                if (!rangeCheckCollider)
                {
                    CreateInteractCollider();
                }

                if (!TryGetComponent<Collider2D>(out var _collider ))
                {
                    _collider = gameObject.AddComponent<PolygonCollider2D>();
                }
                _collider.isTrigger = true;
            }

            gameObject.layer = LayerMask.NameToLayer("Object");
            gameObject.tag = "Object";
        }
        

        private void Start()
        {
            Init();
        }

        public virtual void Init()
        {
            if (isInit) return;
            
            if (!audioSource)
            {
                var _mixer = Resources.Load("Mixer") as AudioMixer;
                audioSource = gameObject.GetComponent<AudioSource>();
                audioSource.outputAudioMixerGroup = _mixer.FindMatchingGroups("SFX")[0];
            }

            if (!isUI)
            {
                spriteRenderer = GetComponent<SpriteRenderer>();
                if (spriteRenderer)
                {
                    startColor = spriteRenderer.color;
                }
                if (!rangeCheckCollider)
                {
                    CreateInteractCollider();
                }
                
                rangeCheckCollider.gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
                rangeCheckCollider.OnPlayerEnter += PlayerEnter;
                rangeCheckCollider.OnPlayerExit += PlayerExit;
            }

            OnPlayerClicked += PlayerClicked;

            this.gameObject.layer = LayerMask.NameToLayer("Object");
            isInit = true;
        }

        public virtual void OnClick(int _inputID)
        {
            if (GameManager.Instance.IsInteracting && GameManager.Instance.GameIsPaused) return;

            if (_inputID == InputManager.RIGHT_CLICK)
            {
                switch (playerInRange)
                {
                    case true when !specificChar:
                    case true when specificChar && GameManager.Instance.CurrentChar == interactableChar:
                        OnPlayerClicked?.Invoke();
                        break;
                    case true when specificChar && GameManager.Instance.CurrentChar != interactableChar:
                        Debug.LogFormat("Only {0} can interact this!", interactableChar);
                        break;
                    default:
                        Debug.Log("Player not in interactable range.");
                        break;
                }
            }
        }

        protected virtual void PlayerClicked()
        {
            //Debug.Log(gameObject.name);
            if (interactSound)
            {
                audioSource.PlayOneShot(interactSound);
            }

            if (shake)
            {
                transform.Shake(shakeTween);
            }
        }
        

        private void PlayerEnter()
        {
            playerInRange = true;
            if (isDebug)
            {
                spriteRenderer.color = Color.green;
            }
        }

        private void PlayerExit()
        {
            playerInRange = false;
            if (isDebug)
            {
                spriteRenderer.color = startColor;
            }
        }

        private void CreateInteractCollider()
        {
            rangeCheckCollider = GetComponentInChildren<RangeCheckCollider>();
            if (!rangeCheckCollider)
            {
                var _p = Resources.Load("Prefabs/InteractCollider") as GameObject;
                rangeCheckCollider = Instantiate(_p, transform).GetComponent<RangeCheckCollider>();
            }
            var _sprite = GetComponent<SpriteRenderer>();
            rangeCheckCollider.Init(_sprite);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (!isUI) return;
            
            if (!specificChar || specificChar && GameManager.Instance.CurrentChar == interactableChar)
            {
                Debug.Log($"Click {gameObject.name}");
                //PlayerClicked();
                OnPlayerClicked?.Invoke();
            }
            else if (specificChar && GameManager.Instance.CurrentChar != interactableChar)
            {
                Debug.LogFormat("Only {0} can interact this!", interactableChar);
            }
        }
    }
}
