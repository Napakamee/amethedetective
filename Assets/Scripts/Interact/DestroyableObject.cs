using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class DestroyableObject : MonoBehaviour, IDamageable
{
    [SerializeField] [ReadOnly] private SpriteRenderer renderer;
    [SerializeField] [ReadOnly] private BoxCollider2D collider2D;
    [SerializeField] private int hp = 3;
    [SerializeField] private AudioClip damageSfx;
    [SerializeField] private AudioClip destroySfx;
    [SerializeField] private ParticleSystem destroyParticle;
    private AudioSource audioSource;

    private void OnValidate()
    {
        renderer = GetComponent<SpriteRenderer>();
        collider2D = GetComponent<BoxCollider2D>();
    }

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void ApplyDamage(int _amount, float _pos)
    {
        hp -= _amount;
        transform.Shake();
        audioSource.PlayOneShot(damageSfx);
        if (hp <= 0)
        {
            ObjectDestroy();
        }
    }

    private void ObjectDestroy()
    {
        audioSource.PlayOneShot(destroySfx);
        if (destroyParticle)
        {
            var _particle = Instantiate(destroyParticle);
            _particle.transform.position = this.transform.position;
        }

        renderer.enabled = false;
        collider2D.enabled = false;
    }
}
