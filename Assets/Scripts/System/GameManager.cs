using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{
    [SerializeField] private PlayerData playerData;
    
    [SerializeField] private bool isUnlockMagnifier = false;
    [SerializeField] private bool isUnlockGun = false;
    private bool isInteracting = false;
    private bool isUsingMagnifier = false;
    private bool gameIsPause = false;
    private bool isComplete = false;
    private bool isResetPlayerData = true;
    private bool isPlayDialog = true;
    [SerializeField] [ReadOnly] private List<CharacterEnum> playableChar = new List<CharacterEnum>();
    [SerializeField][ReadOnly] private CharacterEnum currentChar;
    [SerializeField] private List<ObjectiveData> interactObjectives = new List<ObjectiveData>();
    

    private SceneLoader sceneLoader;
    #region Public

    public bool IsGameOver = false;

    public bool IsInteracting { get => isInteracting; set => isInteracting = value; }
    public List<ObjectiveData> InteractObjectives { get => interactObjectives; set => interactObjectives = value; }
    public CharacterEnum CurrentChar { get => currentChar; set => currentChar = value; }
    public bool GameIsPaused { get => gameIsPause; set => gameIsPause = value; }
    public bool IsUsingMagnifier { get => isUsingMagnifier; set => isUsingMagnifier = value; }

    public bool IsUnlockMagnifier => isUnlockMagnifier;
    public bool IsUnlockGun => isUnlockGun;

    public bool IsResetPlayerData { get => isResetPlayerData; set => isResetPlayerData = value; }

    public List<CharacterEnum> PlayableChar { get => playableChar; set => playableChar = value; }

    public bool IsComplete
    {
        get => isComplete;
        set => isComplete = value;
    }

    public PlayerData Data
    {
        get => playerData;
        set => playerData = value;
    }

    public bool IsPlayDialog
    {
        get => isPlayDialog;
        set => isPlayDialog = value;
    }

    #endregion

    public override void Init()
    {
        base.Init();
        UnloadScene();
        isUsingMagnifier = false;
        isComplete = false;
        IsGameOver = false;
        playerData = Resources.Load<PlayerData>("PlayerData");
        if (!isResetPlayerData)
        {
            isUnlockMagnifier = playerData.UnlockedMagnifier;
            isUnlockGun = playerData.UnlockedGun;
        }
        else
        {
            isUnlockMagnifier = false;
            isUnlockGun = false;
        }
        
        sceneLoader = FindObjectOfType<SceneLoader>();

        foreach (var _objective in sceneLoader.ExtraObjectives)
        {
            if (!interactObjectives.Contains(_objective))
            {
                interactObjectives.Add(_objective);
            }
        }
    }

    public void UnlockGun()
    {
        isUnlockGun = true;
        playerData.UnlockedGun = true;
    }
    
    public void UnlockMagnifier()
    {
        isUnlockMagnifier = true;
        playerData.UnlockedMagnifier = true;
        var _magController = FindObjectOfType<MagnifierController>();
        _magController.Unlock();
    }

    public void StageComplete()
    {
        sceneLoader.StageComplete();
        isComplete = true;
        Debug.Log("Stage Complete!");
    }
    
    public bool IsClear()
    {
        return interactObjectives.All(_x => _x.IsComplete);
    }


    public void UnloadScene()
    {
        interactObjectives.Clear();
    }

    public void SortObjective()
    {
        interactObjectives = interactObjectives.OrderBy(x => x.ID).ToList();
    }

    public void UpdateStageState(int _stageIndex)
    {
        playerData.UpdateStageStateComplete(_stageIndex);
        if (_stageIndex < 2)
        {
            playerData.UnlockState(_stageIndex + 1);
        }
    }

    public void NextScene(string _sceneName)
    {
        if (isComplete)
        {
            SceneManager.LoadScene(_sceneName);
        }
    }
}
