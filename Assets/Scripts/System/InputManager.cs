using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public interface IClickable
{
    void OnClick(int _inputID);
}

public class InputManager : MonoBehaviour
{
    public Action<InputAction.CallbackContext> OnJump;
    public Action<InputAction.CallbackContext> OnMove;
    public Action<InputAction.CallbackContext> OnEsc;
    public Action<InputAction.CallbackContext> OnMoveMouse;
    public Action<InputAction.CallbackContext> OnLClick;
    public Action<InputAction.CallbackContext> OnRClick;
    public Action<InputAction.CallbackContext> OnChangeChar;
    public Action<InputAction.CallbackContext> OnToggleMagnifier;
    public Action OnFinishedInit;
    public const int LEFT_CLICK = 0;
    public const int RIGHT_CLICK = 1;
    
    [SerializeField]
    private Camera gameCamera; 
    
    private PlayerInputActions playerInput;
    
    public PlayerInputActions PlayerInput => playerInput;

    protected void Start()
    {
        //Debug.Log("Start setup input");
        playerInput = new PlayerInputActions();
        playerInput.Movement.Jump.performed += _context => OnJump?.Invoke(_context);
        playerInput.ShortcutKeys.Esc.performed += _context => OnEsc?.Invoke(_context);
        playerInput.Movement.Move.performed += _context => OnMove?.Invoke(_context);

        playerInput.MouseInput.MousePosition.performed += _context => OnMoveMouse?.Invoke(_context);

        playerInput.ShortcutKeys.ChangeChar.performed += _context => OnChangeChar?.Invoke(_context);
        
        playerInput.MouseInput.LeftClick.performed += _context => {
            Click2D(0);
            Click3D(0);
            OnLClick?.Invoke(_context);
        };
        playerInput.MouseInput.RightClick.performed += _context =>
        {
            Click2D(1);
            Click3D(1);
            OnRClick?.Invoke(_context);
        };
        playerInput.ShortcutKeys.ToggleMagnifier.performed += _context => OnToggleMagnifier?.Invoke(_context);
        
        playerInput.Enable();
        //Debug.Log("Finished setup input");
        OnFinishedInit?.Invoke();
    }
    
    private void Click2D(int _inputID)
    {
        Vector2 _coor = Mouse.current.position.ReadValue();
        Ray _ray = gameCamera.ScreenPointToRay(_coor);
        RaycastHit2D _hit = Physics2D.GetRayIntersection(_ray, Mathf.Infinity);
        if (_hit) 
        {
            _hit.collider.GetComponent<IClickable>()?.OnClick(_inputID);
            //Debug.Log("Hit " + _hit.collider.name);
        }
    }
    
    private void Click3D(int _inputID)
    {
        Vector2 _coor = Mouse.current.position.ReadValue();
        Ray _ray = gameCamera.ScreenPointToRay(_coor);
        RaycastHit _hit;
        if (Physics.Raycast(_ray, out _hit)) 
        {
            _hit.collider.GetComponent<IClickable>()?.OnClick(_inputID);
            //Debug.Log("Hit " + _hit.collider.name);
        }
    }
}
