using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Dialog
{
    public class DialogStoryUI : DialogBase
    {
        public Action<string> OnFindNewSpeaker;
        
        [Header("StoryUI")] 
        [SerializeField] private List<YarnProgram> dialogStoryList;

        [SerializeField] private Image cutscene;
        [SerializeField] private TextMeshProUGUI speakerName;
        [SerializeField] private Button nextButton;
        [SerializeField] private Button autoButton;
        [SerializeField] private Button skipButton;

        private List<RectTransform> loadedChar = new List<RectTransform>();
        private List<string> speakerNames = new List<string>();

        private Dictionary<string, Vector2> charPos = new Dictionary<string, Vector2>()
        {
            {"left", new Vector2(0.25f,0.5f)},
            {"middle", new Vector2(0.5f,0.5f)},
            {"right", new Vector2(0.75f, 0.5f)}
        };
        
        public override void Init()
        {
            base.Init();
            
            foreach (var _storyData in dialogStoryList)
            {
                Add(_storyData);
            }
            //Change line when click and turn dialog mode to Click.
            nextButton.onClick.AddListener(delegate
            {
                DialogUI.MarkLineComplete();
                dialogMode = DialogMode.Click;
            });
            
            skipButton.onClick.AddListener(delegate
            {
                DialogUI.DialogueComplete();
                NodeComplete();
            });
            
            //Toggle dialog mode between Auto/Click.
            autoButton.onClick.AddListener(delegate
            {
                dialogMode = dialogMode == DialogMode.Click ? DialogMode.Auto : DialogMode.Click;
                if (isLineFinished)
                {
                    DialogUI.OnLineFinishDisplaying?.Invoke();
                }
            });
            //DialogUI.OnFindNewSpeaker += LoadCharacter;
            GetSpeakers();
            DialogUI.OnSpeakerChanged += ChangeSpeaker;

            OnNodeComplete += delegate(string _s) { NodeComplete(); };
        }

        protected override void SetCommand()
        {
            base.SetCommand();
            AddCommandHandler("set_cutscene", SetCutScene);
            AddCommandHandler("hide_cutscene", HideCutScene);
            AddCommandHandler("show_char", ShowChar);
            AddCommandHandler("hide_char", HideChar);
            
            AddCommandHandler("set_pos", SetSpeakerPos);
        }

        private void GetSpeakers()
        {
            List<string> _dialogStringList = dialogStoryList.Select(_yarnProgram => _yarnProgram.baseLocalisationStringTable.text).ToList();

            foreach (var _text in _dialogStringList)
            {
                StringReader _reader = new StringReader(_text);
                string _line;
                while ((_line = _reader.ReadLine()) != null)
                {
                    _line = TagReader.RemoveYarnTextAssetPrefix(_line);
                    TagReader.SplitSpeakerName(_line, out string _speaker, out _);
                    if (!speakerNames.Contains(_speaker) && _speaker != null)
                    {
                        speakerNames.Add(_speaker);
                        LoadCharacter(_speaker);
                        OnFindNewSpeaker?.Invoke(_speaker);
                        //Debug.LogFormat("Get {0}", _speaker);
                    }
                    
                    //Debug.LogFormat(_line);
                }
            }
            //Debug.Log("Finished loading speakers.");
            SetCommand();
        }
        
        private void LoadCharacter(string _char)
        {
            
            GameObject _speaker = new GameObject(_char);
            var _rect = _speaker.AddComponent<RectTransform>();
            
            //Set speaker's parent to DialogContainer.
            _rect.SetParent(DialogUI.DialogContainer.transform); 
            _rect.transform.localScale = Vector3.one; 
            _rect.transform.localPosition = Vector3.zero;

            //Get speaker data.
            var _data = FetchAsset<SpeakerData>(_char);
            var _dialogSpeaker = _speaker.AddComponent<DialogSpeaker>();
            var _image = _speaker.AddComponent<Image>();
            
            _dialogSpeaker.Init(_data, _image, _rect);
            _dialogSpeaker.SetImage(SpeakerData.NATURAL);
            _image.enabled = false;
            
            loadedChar.Add(_rect);
            _rect.transform.SetAsFirstSibling();
            //Debug.LogFormat("Loaded {0}", _char);
        }

        #region Command

        private void SetCutScene(string[] _param)
        {
            cutscene.enabled = true;
            cutscene.sprite = FetchAsset<Sprite>(_param[0]);
        }

        private void HideCutScene(string[] _param)
        {
            cutscene.enabled = false;
        }

        private void ShowChar(string[] _param)
        {
            var _char = GetCharRect(_param[0]);

            if (_param.Length <= 2)
            {
                Debug.LogError("show_char need 3 parameters <<show_char Ame natural left>>");
                return;
            }
            
            if (!string.IsNullOrWhiteSpace(_param[1]))
            {
                if (_char.TryGetComponent<DialogSpeaker>(out var _speaker))
                {
                    _speaker.SetImage(_param[1]);
                }
            }
            if (!string.IsNullOrWhiteSpace(_param[2]))
            {
                SetSpeakerPos(new []{_param[0], _param[2]});
            }
            
            _char.GetComponent<Image>().enabled = true;
        }

        private void HideChar(string[] _param)
        {
            var _char = GetCharRect(_param[0]);
            _char.GetComponent<Image>().enabled = false;
        }
        
        private void SetSpeakerPos(string[] _param)
        {
            var _char = GetCharRect(_param[0]);
            var _spritePos = _param[1];
            _spritePos = _spritePos.ToLower();
            switch (_spritePos)
            {
                case "left": 
                    _char.anchorMax = charPos["left"];
                    _char.anchorMin = charPos["left"];
                    
                    break;
                case "middle":
                    _char.anchorMax = charPos["middle"];
                    _char.anchorMin = charPos["middle"];
                    break;
                case "right":
                    _char.anchorMax = charPos["right"];
                    _char.anchorMin = charPos["right"];
                    break;
            }
            _char.anchoredPosition = new Vector2(0, -100);
        }

        private RectTransform GetCharRect(string _name)
        {
            var _char = FetchAsset<RectTransform>(_name);
            if(!_char)
            {
                Debug.LogErrorFormat("{0} doesn't exit in database.", _name);
                return null;
            }
            return _char;
        }

        #endregion
        
        protected override T FetchAsset<T>(string _assetName)
        {
            if (typeof(T) == typeof(RectTransform))
            {
                foreach (var _rect in loadedChar)
                {
                    if (_rect.name == _assetName)
                    {
                        return _rect as T;
                    }
                }
            }
            return base.FetchAsset<T>(_assetName);
        }
        
        public void ChangeSpeaker(string _speaker)
        {
            speakerName.text = _speaker;
        }

        public override void StartDialog(string _startNode)
        {
            base.StartDialog(_startNode);
            GameManager.Instance.IsInteracting = true;
        }

        private void NodeComplete()
        {
            GameManager.Instance.IsInteracting = false;
        }
    }
}
