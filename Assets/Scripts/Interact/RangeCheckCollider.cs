using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeCheckCollider : MonoBehaviour
{
    public Action OnPlayerEnter;
    public Action OnPlayerExit;

    private BoxCollider2D collider;
    public void Init(SpriteRenderer _sprite)
    {
        collider = GetComponent<BoxCollider2D>();
        //collider.center = Vector3.zero;
        collider.size = new Vector3(_sprite.bounds.size.x / transform.lossyScale.x, 
                                    _sprite.bounds.size.y / transform.lossyScale.y, 1);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("PlayerInteractRange"))
        {
            OnPlayerEnter?.Invoke();
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("PlayerInteractRange"))
        {
            OnPlayerExit?.Invoke();
        }
    }
}
