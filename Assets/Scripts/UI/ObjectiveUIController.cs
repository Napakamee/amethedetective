using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveUIController : MonoBehaviour
{
    [SerializeField] private ObjectiveUIItem uiPrefab;

    private void Start()
    {
        foreach (var _objective in GameManager.Instance.InteractObjectives)
        {
            if (_objective)
            {
                ObjectiveUIItem _obj = Instantiate(uiPrefab, this.transform, false);
                _obj.Init(_objective);
                _objective.OnComplete += _obj.UpdateProgress;
                _objective.OnComplete += delegate {
                    if (GameManager.Instance.IsClear())
                    {
                        GameManager.Instance.StageComplete();
                    }
                };
            }
        }
    }
}
