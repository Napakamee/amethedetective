using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingMenu : MonoBehaviour
{
    private Canvas canvas;

    public void OnClick_Back()
    {
        canvas = gameObject.GetComponent<Canvas>();
        GameMenuManager.OpenMenu(Menu.MAINMENU, canvas);
    }
}
