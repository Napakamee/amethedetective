using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.InputSystem;

[Serializable]
public class CharDataDict : UnitySerializedDictionary<CharacterEnum, GameObject>{}

public enum CharacterEnum
{
    Watson = 0, Gura = 1, Inanis = 2, Kiara, Mori
}

public class Player : PlayerBase
{
    [SerializeField]
    private CharDataDict charData = new CharDataDict();

    private List<CharacterEnum> playableChar = new List<CharacterEnum>(); 

    [SerializeField][ReadOnly] private CharacterEnum currentChar;

    public Action<CharacterEnum> OnChangeChar;
    
    public override void Init()
    {
        base.Init();

        playableChar = GameManager.Instance.PlayableChar;

        UpdateChar();
        var _inputManager = FindObjectOfType<InputManager>();
        _inputManager.OnChangeChar += SwapChar;

        var _sceneLoader = FindObjectOfType<SceneLoader>();
        _sceneLoader.OnUnlockedChar += UnlockChar;
    }

    private void SwapChar(InputAction.CallbackContext _context)
    {
        if (_context.ReadValue<float>() >= 1.0f)
        {
            var _next = ((int)currentChar + 1) % charData.Count;
            
            if (playableChar.Contains((CharacterEnum) _next))
                ChangeCharacter((CharacterEnum) _next);
            else
            {
                _next = ((int)_next + 1) % charData.Count;
                if (playableChar.Contains((CharacterEnum) _next))
                    ChangeCharacter((CharacterEnum) _next);
            }
            
        }
        else if (_context.ReadValue<float>() <= -1.0f)
        {
            var _prev = ((int)currentChar + charData.Count - 1) % charData.Count;
            if (playableChar.Contains((CharacterEnum) _prev))
                ChangeCharacter((CharacterEnum) _prev);
            else
            {
                _prev = ((int)_prev + charData.Count - 1) % charData.Count;
                if (playableChar.Contains((CharacterEnum) _prev))
                    ChangeCharacter((CharacterEnum) _prev);
            }
        }
    }

    public void UnlockChar(CharacterEnum _char)
    {
        if (!playableChar.Contains(_char))
        {
            playableChar.Add(_char);
            Debug.Log($"Unlocked {_char}");
        }
    }
    
    public void ChangeCharacter(CharacterEnum _characterData)
    {
        isAttacking = false;
        foreach (var _charObj in charData.Values)
        {
            _charObj.SetActive(false);
        }
        currentChar = _characterData;
        GameManager.Instance.CurrentChar = currentChar;
        charData[_characterData].SetActive(true);
        UpdateChar();
        OnChangeChar?.Invoke(currentChar);
    }

    private void UpdateChar()
    {
        switch (currentChar)
        {
            case CharacterEnum.Watson:
                OnLeftClick = WatsonLeftClick;
                break;
            case CharacterEnum.Gura:
                OnLeftClick = GuraLeftClick;
                attackBox.Damage = 5;
                break;
            case CharacterEnum.Inanis:
                OnLeftClick = InanisLeftClick;
                inaAttackBox.Damage = 4;
                break;
            default:
                OnLeftClick = delegate { };
                break;
        }

        playerAnimator = charData[currentChar].GetComponent<Animator>();
        currentState = "";
    }

    private void WatsonLeftClick()
    {
        if (!GameManager.Instance.IsUnlockGun)
        {
            Debug.Log("Need to collect gun first");
            return;
        }
        
        playerSound.PlaySound(playerSound.WatsonAttackSound);
        isAttacking = true;
        if (PoolManager.Instance.bulletPool.Count > 1)
        {
            Bullet.SpawnBullet(attackBox.transform.position, 15, isFacingRight ? 1:-1, BulletType.Player);
        }
        else
        {
            Bullet.CreateBullet(attackBox.transform.position, 15, isFacingRight ? 1:-1, BulletType.Player);
        }

        if (!isOnsecondary)
        {
            ChangeAnimationState(ATTACK, delegate { isAttacking = false; });
        }
    }

    private void GuraLeftClick()
    {
        isAttacking = true;
        ChangeAnimationState(ATTACK, delegate { isAttacking = false; });
        playerSound.PlaySound(playerSound.GuraAttackSound);
        attackBox.Show();
        
        StopCoroutine(HideWeapon());
        StartCoroutine(HideWeapon());
        //Debug.Log("GuraLeftClick");
    }

    private void InanisLeftClick()
    {
        isAttacking = true;
        canMove = false;
        rb.velocity = Vector2.zero;
        playerSound.PlaySound(playerSound.InaAttackSound);
        WaitFor(0.8f, delegate
        {
            inaAttackBox.Show();
        });
        //Debug.Log("InanisLeftClick");
        ChangeAnimationState(ATTACK, delegate 
        { 
            inaAttackBox.Hide();
            isAttacking = false;
            canMove = true;
        });
    }

    private void LeftClick(Action _isComplete = null)
    {
        
    }

    private void WaitFor(float _waitTime, Action _onComplete = null)
    {
        StartCoroutine(IEWaitFor( _waitTime, _onComplete));
    }

    IEnumerator IEWaitFor(float _waitTime, Action _onComplete = null)
    {
        yield return new WaitForSeconds(_waitTime);
        _onComplete?.Invoke();
    }

    private IEnumerator HideWeapon()
    {
        yield return new WaitForSeconds(0.1f);
        attackBox.Hide();
    }
}
