using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Audio;

[RequireComponent(typeof(AudioSource), typeof(Rigidbody2D), typeof(BoxCollider2D))]
public class EnemyBase : MonoBehaviour, IDamageable
{
    private enum State { Moving, FoundPlayer, Knockback, Dead }

    [SerializeField] protected GameObject enemyBody;
    
    [SerializeField][FoldoutGroup("Stat")] private int health = 4;

    [SerializeField] [FoldoutGroup("Stat")]
    protected float moveSpeed = 3f;
    protected float damageCooldown;

    [SerializeField][FoldoutGroup("Stat")] private Vector2 knockbackSpeed = new Vector2(3, 2);
    
    [SerializeField][FoldoutGroup("Checker")]
    private Transform groundCheck, wallCheck, playerCheck;
    
    [SerializeField][FoldoutGroup("Checker")]
    private float groundCheckDistance = 0.5f, wallCheckDistance = 0.1f, playerCheckDistance;
    
    [SerializeField] private AudioClip damageSfx;
    [SerializeField] private AudioClip destroySfx;
    [SerializeField] private ParticleSystem destroyParticle;

    
    private float knockbackStartTime;
    [SerializeField] protected int facingDirection = 1, damageDirection;
    protected Rigidbody2D rb;
    private Vector2 movement;
    private bool groundDetected, wallDetected, foundPlayer;
    protected bool canAttack;
    [SerializeField][ReadOnly] private State currentState;
    private LayerMask groundMask;
    private LayerMask playerMask;
    
    protected AudioSource audioSource;
    protected Player player;

    [Button]
    public void SetUpEnemy()
    {
        if (!enemyBody)
        {
            var _bodyPrefab = Resources.Load("Prefabs/Enemy/Body") as GameObject;
            enemyBody = transform.Find("Body").gameObject;
            if (!enemyBody)
            {
                enemyBody = Instantiate(_bodyPrefab, transform);
            }
            enemyBody.name = "Body";
        }

        knockbackSpeed = new Vector2(4, 2);
        if (!groundCheck)
        {
            groundCheck = enemyBody.transform.Find("GroundCheck").transform;
        }

        if (!wallCheck)
        {
            wallCheck = enemyBody.transform.Find("WallCheck").transform;
        }

        if (!playerCheck)
        {
            playerCheck = enemyBody.transform.Find("PlayerCheck").transform;
        }

        var _rb = GetComponent<Rigidbody2D>();
        _rb.SetUpRigidBody();

        gameObject.layer = LayerMask.NameToLayer("Enemy");
        gameObject.tag = "Enemy";
    }

    protected virtual void Start()
    {
        groundMask = LayerMask.GetMask("Floor");
        playerMask = LayerMask.GetMask("Player");
        player = FindObjectOfType<Player>();
        audioSource = GetComponent<AudioSource>();
        var _mixer = Resources.Load<AudioMixer>("Mixer");
        audioSource.outputAudioMixerGroup = _mixer.FindMatchingGroups("SFX")[0];
        rb = GetComponent<Rigidbody2D>();
        canAttack = true;
    }

    
    void Update()
    {
        UpdateState();
        UpdateRaycast();
    }

    #region MovingState

    protected virtual void EnterMoving(){ }

    protected virtual void UpdateMoving()
    {
        FlipRotate();
        
        groundDetected = Physics2D.Raycast(groundCheck.position, Vector2.down, groundCheckDistance, groundMask);
        wallDetected = Physics2D.Raycast(wallCheck.position, transform.right, wallCheckDistance, groundMask);
        
        if(!groundDetected || wallDetected)
        {
            Flip();
        }
        else
        {
            movement.Set(moveSpeed * facingDirection, rb.velocity.y);
            rb.velocity = movement;
        }
    }
    
    protected virtual void ExitMoving(){ }
    
    protected void Flip()
    {
        facingDirection *= -1;
    }

    protected void FlipRotate()
    {
        var _yRot = facingDirection > 0 ? 0f : 180f;
        
        enemyBody.transform.localRotation = Quaternion.Slerp(
            enemyBody.transform.localRotation,
            Quaternion.Euler(0,_yRot,0),
            Time.deltaTime * 50);
    }

    #endregion

    #region FoundPlayerState

    protected virtual void EnterFound() { }
    protected virtual void UpdateFound() { }
    protected virtual void ExitFound() { }

    #endregion

    #region KnockbackState

    protected virtual void EnterKnockback()
    {
        knockbackStartTime = Time.time;
        movement.Set(knockbackSpeed.x * damageDirection, knockbackSpeed.y);
        rb.velocity = movement;
    }
    
    protected virtual void UpdateKnockback()
    {
        if(Time.time >= knockbackStartTime + 0.1f)
        {
            SwitchState(State.Moving);
        }
    }
    
    protected virtual void ExitKnockback(){ }

    #endregion

    #region DeathState

    protected virtual void EnterDead()
    {
        audioSource.PlayOneShot(destroySfx);
        if (destroyParticle)
        {
            var _particle = Instantiate(destroyParticle);
            _particle.transform.position = this.transform.position;
        }

        enemyBody.SetActive(false);
        StartCoroutine(DestroyDelay());

    }
    protected virtual void UpdateDead(){ }
    protected virtual void ExitDead(){ }

    #endregion

    #region State

    private void SwitchState(State _state)
    {
        switch (currentState)
        {
            case State.Moving:
                ExitMoving();
                break;
            case State.Knockback:
                ExitKnockback();
                break;
            case State.Dead:
                ExitDead();
                break;
            case State.FoundPlayer:
                ExitFound();
                break;
        }

        switch (_state)
        {
            case State.Moving:
                EnterMoving();
                break;
            case State.Knockback:
                EnterKnockback();
                break;
            case State.Dead:
                EnterDead();
                break;
            case State.FoundPlayer:
                EnterFound();
                break;
        }

        currentState = _state;
    }

    private void UpdateState()
    {
        switch (currentState)
        {
            case State.Moving:
                UpdateMoving();
                break;
            case State.Knockback:
                UpdateKnockback();
                break;
            case State.Dead:
                UpdateDead();
                break;
            case State.FoundPlayer:
                UpdateFound();
                break;
        }
    }

    #endregion

    private void UpdateRaycast()
    {
        foundPlayer = Physics2D.Raycast(transform.position, transform.right * facingDirection, playerCheckDistance, playerMask);

        SwitchState(foundPlayer ? State.FoundPlayer : State.Moving);
    }
    
    private void OnCollisionStay2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            DealDamage(other.gameObject);
        }
        
    }

    private void DealDamage(GameObject _target)
    {
        if (!canAttack) return;
        
        _target.GetComponent<IDamageable>()?.ApplyDamage(1, transform.position.x);
        canAttack = false;
        StartCoroutine(DamageCooldown());
    }

    private IEnumerator DamageCooldown()
    {
        yield return new WaitForSeconds(damageCooldown);
        canAttack = true;
    }
    
    public void ApplyDamage(int _amount, float _pos)
    {
        health -= _amount;
        damageDirection = _pos > transform.position.x ? -1 : 1;
        
        if(health > 0)
        {
            SwitchState(State.Knockback);
            FlashRed();
        }
        else if(health <= 0)
        {
            SwitchState(State.Dead);
        }
    }

    private void FlashRed()
    {
        foreach (var _renderer in GetComponentsInChildren<SpriteRenderer>())
        {
            _renderer.Flash(Color.red, 0.1f);
        }
    }

    protected virtual void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawLine(groundCheck.position, new Vector2(groundCheck.position.x, groundCheck.position.y - groundCheckDistance));
        Gizmos.DrawLine(wallCheck.position, new Vector2(wallCheck.position.x + wallCheckDistance, wallCheck.position.y));
        Gizmos.color = Color.green;
        Gizmos.DrawLine(playerCheck.position, new Vector2(playerCheck.position.x + playerCheckDistance * facingDirection, playerCheck.position.y));
    }

    private IEnumerator DestroyDelay()
    {
        yield return new WaitForSeconds(0.3f);
        Destroy(this.gameObject);
    }
}
