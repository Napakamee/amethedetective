using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/ObjectiveData")]
public class ObjectiveData : ScriptableObject
{
    public Action OnComplete;
    
    [SerializeField] private string name;
    [SerializeField] private int id;
    [SerializeField][TextArea] private string description;
    [SerializeField] private Sprite sprite;
    [SerializeField] private bool isComplete;
    
    public string Name => name;

    public Sprite Sprite => sprite;
    
    public string Description => description;
    public bool IsComplete
    {
        get => isComplete;
        set => isComplete = value;
    }

    public int ID => id;

    public void Init()
    {
        ResetProgress();
    }

    public void Complete()
    {
        isComplete = true;
        OnComplete?.Invoke();
        Debug.Log(name + "Complete");
    }

    public void GenerateObjectiveData(Sprite _sprite, string _name)
    {
#if UNITY_EDITOR
        sprite = _sprite;
        name = _name;
        EditorUtility.SetDirty(this);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
#endif
    }

    [Button]
    public void ResetProgress()
    {
        isComplete = false;
    }
}
