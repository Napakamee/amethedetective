using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ObjectiveUIItem : MonoBehaviour
{
    [SerializeField] private float iconSize = 100f;
    [SerializeField] private TextMeshProUGUI name;
    [SerializeField] private TextMeshProUGUI description;
    [SerializeField] private Image missionImage;
    [SerializeField] private Image checkMark;

    private ObjectiveData objective;

    private RectTransform rectTransform;

    public void Init(ObjectiveData _objective)
    {
        rectTransform = GetComponent<RectTransform>();
        objective = _objective;
        name.text = objective.Name;
        description.text = objective.Description;
        missionImage.sprite = objective.Sprite;
        checkMark.enabled = false;
        var _iconRect = missionImage.GetComponent<RectTransform>();
        missionImage.SetNativeSize();
        UIUtilities.SetPreferredSize(_iconRect, iconSize);
    }

    
    
    public void UpdateProgress()
    {
        if (objective.IsComplete)
        {
            if (checkMark)
            {
                checkMark.enabled = true;
            }
        }
    }

}
