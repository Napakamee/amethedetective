using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class GroundCheck : MonoBehaviour
{
    private LayerMask groundMask;

    private int playerLayer, platformLayer;

    [SerializeField] private bool isGrounded;
    private bool dropping;
    private bool pressDown;
    
    public bool IsGrounded => isGrounded;

    public bool Dropping => dropping;

    public bool PressDown => pressDown;

    private void Awake()
    {
        playerLayer = LayerMask.NameToLayer("Player");
        platformLayer = LayerMask.NameToLayer("DropableFloor");
        groundMask += LayerMask.GetMask("Floor");
        groundMask += LayerMask.GetMask("DropableFloor");
    }

    private void Update()
    {
        UpdateInput();
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if(other.CompareTag("Object")) return;
        if (other.TryGetComponent<RangeCheckCollider>(out _)) return;
        
        if (dropping)
        {
            isGrounded = false;
            return;
        }
        isGrounded = other != null && ((1 << other.gameObject.layer) & groundMask) != 0;
        
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if(other.CompareTag("Object")) return;
        if (other.TryGetComponent<RangeCheckCollider>(out _)) return;
        
        isGrounded = false;
    }

    private void UpdateInput()
    {
        pressDown = Input.GetKey(KeyCode.S);

        if (isGrounded && Input.GetKey(KeyCode.S) && Input.GetKeyDown(KeyCode.Space))
        {
            DropDown();
        }
    }

    private void DropDown()
    {
        StopCoroutine(nameof(IEDropDown));
        StartCoroutine(nameof(IEDropDown));
    }

    private IEnumerator IEDropDown()
    {
        dropping = true;
        Physics2D.IgnoreLayerCollision(playerLayer, platformLayer, true);
        yield return new WaitForSeconds(0.5f);
        Physics2D.IgnoreLayerCollision(playerLayer, platformLayer, false);
        dropping = false;

    }
}
