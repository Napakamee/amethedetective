using System.Collections;
using System.Collections.Generic;
using Module.Interact;
using UnityEngine;
using UnityEngine.Events;

public class InteractPlayEvent : InteractableObject
{
    [SerializeField] public UnityEvent OnInteract;
    [SerializeField] private bool oneTime;
    private bool isInteracted = false;
    
    public override void OnClick(int _inputID)
    {
        base.OnClick(_inputID);
        if (oneTime)
        {
            if (!isInteracted)
            {
                OnInteract?.Invoke();
                isInteracted = true;
            }
        }
        else
        {
            Debug.Log("Play OnInteract");
            OnInteract?.Invoke();
        }
        
    }
}
