using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HPUI : MonoBehaviour
{
    [SerializeField] private GameObject hpPrefab;
    [SerializeField] private Transform container;
    [SerializeField] private Queue<GameObject> hp = new Queue<GameObject>();
    void Start()
    {
        var _player = FindObjectOfType<Player>();
        for (int i = 0; i < _player.Hp; i++)
        {
            var _hp = Instantiate(hpPrefab, container);
            hp.Enqueue(_hp);
        }
        
        _player.OnDamaged += delegate
        {
            var _hp = hp.Dequeue();
            Destroy(_hp);
        };
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
