using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraExtension : MonoBehaviour
{
    private static Camera cam => Camera.main;
    private bool isDebug = false;

    public static float GetMaxX()
    {
        return cam.ViewportToWorldPoint(new Vector3(1, 1, 10)).x;
    }

    public static float GetMinX()
    {
        return cam.ViewportToWorldPoint(new Vector3(0, 1, 10)).x;
    }

    public static float GetMaxY()
    {
        return cam.ViewportToWorldPoint(new Vector3(1, 1, 10)).y;
    }

    public static float GetMinY()
    {
        return cam.ViewportToWorldPoint(new Vector3(0, 0, 10)).y;
    }
    private void OnDrawGizmos()
    {
        if(!isDebug) return;
        
        Vector3 _tR = cam.ViewportToWorldPoint(new Vector3(1, 1, 10));
        Vector3 _tL = cam.ViewportToWorldPoint(new Vector3(0, 1, 10));
        Vector3 _bR = cam.ViewportToWorldPoint(new Vector3(1, 0, 10));
        Vector3 _bL = cam.ViewportToWorldPoint(new Vector3(0, 0, 10));
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(_tL, _tR);
        Gizmos.DrawLine(_tR, _bR);
        Gizmos.DrawLine(_bR, _bL);
        Gizmos.DrawLine(_bL, _tL);
        
    }
}
