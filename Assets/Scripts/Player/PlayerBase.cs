using System;
using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;

[RequireComponent(typeof(PlayerSoundController))]
public class PlayerBase : MonoBehaviour, IDamageable
{
    public Action OnMove;
    public Action OnDamaged;
    protected Action OnLeftClick;
    protected Action OnRightClick;

    #region Inspector

    [SerializeField][ReadOnly] protected PlayerSoundController playerSound;
    [SerializeField][ReadOnly] private Transform playerTransform;
    [SerializeField] private GameObject charContainer;
    [SerializeField] private int hp = 5;
    
    [SerializeField][FoldoutGroup("PlayerController")] protected PlayerAttackBox attackBox;
    [SerializeField][FoldoutGroup("PlayerController")] protected PlayerAttackBox inaAttackBox;
    [SerializeField][FoldoutGroup("PlayerController")] private Vector2 knockbackForce;
    [SerializeField][FoldoutGroup("PlayerController")] private float knockbackTime = 0.1f;
    [SerializeField][FoldoutGroup("PlayerController")] private float invincibleTime = 1f;
    [SerializeField][FoldoutGroup("PlayerController")] private float playerSpeed = 5f;
    [SerializeField][FoldoutGroup("PlayerController")] private float jumpForce = 10f;
    [SerializeField][FoldoutGroup("PlayerController")] private float fallMultiplier = 2.5f;
    [SerializeField][FoldoutGroup("PlayerController")] private float holdJumpMultiplier = 2f;
    [SerializeField][FoldoutGroup("PlayerController")] private float climbSpeed = 4f;
    [SerializeField][FoldoutGroup("PlayerController")] private float leftClickCd = 0;

    #endregion
    
    private GroundCheck groundCheck;
    private PlayerInputActions playerInput;
    private Vector2 moveInput;
    private Vector2 mouseInput;
    private InputAction jump;
    private InputAction climbUp;
    private InputAction climbDown;
    protected InputAction leftClick;
    protected InputAction rightClick;

    #region Bool

    protected bool isFacingRight = true;
    protected bool canClick = true;
    protected bool canMove = true;
    protected bool isKnockback = false;
    protected bool isJump = false;
    protected bool isInvincible = false;
    protected bool isAttacking = false;
    protected bool isOnsecondary = false;
    protected bool insideLadder = false;

    #endregion
    
    private float abilityCD = 0.3f;
    
    private Transform curser;
    protected Rigidbody2D rb;
    protected Camera cam;
    protected Animator playerAnimator;
    [SerializeField][ReadOnly] protected string currentState;

    #region AnimationString

    protected const string IDLE = "Idle";
    protected const string IDLE2 = "Idle2";
    protected const string ATTACK = "Attack";
    protected const string JUMP = "Jump";
    protected const string JUMP2 = "Jump2";
    protected const string RUN = "Run";
    protected const string RUN2 = "Run2";
    protected const string SIT = "Sit";
    protected const string SIT2 = "Sit2";

    #endregion

    private InputManager inputManager;

    public int Hp => hp;

    private void OnValidate()
    {
        if (!playerTransform)
        {
            playerTransform = gameObject.GetComponent<Transform>();
        }

        if (!playerSound)
        {
            playerSound = gameObject.GetComponent<PlayerSoundController>();
        }
    }

    private void Awake()
    {
        inputManager = FindObjectOfType<InputManager>();
        inputManager.OnFinishedInit += BindInput;
    }

    private void Start()
    {
        cam = Camera.main;
        groundCheck = GetComponentInChildren<GroundCheck>();
        curser = GameObject.Find("Crosshair").transform;
        Init();
    }

    private void BindInput()
    {
        playerInput = inputManager.PlayerInput;

        var _gameManager = GameManager.Instance;
        
        inputManager.OnMoveMouse += _context => mouseInput = _context.ReadValue<Vector2>(); 
        
        inputManager.OnMove += _context => moveInput = _context.ReadValue<Vector2>(); 
        
        inputManager.OnLClick += _context =>
        {
            if(_gameManager.IsInteracting || _gameManager.GameIsPaused || _gameManager.IsUsingMagnifier) return;
            isFacingRight = curser.position.x - transform.position.x > 0;
            StartCoroutine(DelayInput(OnLeftClick));
        };
        
        inputManager.OnRClick += _context =>
        {
            if(_gameManager.IsInteracting || _gameManager.GameIsPaused) return;
            abilityCD = leftClickCd;
            isFacingRight = curser.position.x - transform.position.x > 0;
            StartCoroutine(DelayInput(OnRightClick));
        };
        
        inputManager.OnJump += _context =>
        {
            Jump();
        };
        
        
        //playerInput.Enable();
        //Debug.Log("Finished binding input");
    }
    
    public virtual void Init()
    {
        if (!playerTransform)
        {
            playerTransform = gameObject.GetComponent<Transform>();
        }
        rb = GetComponent<Rigidbody2D>();
        
        attackBox.Hide();
        inaAttackBox.Hide();
    }

    #region UnityEvent

    private void OnEnable()
    {
        playerInput?.Enable();

    }

    private void OnDisable()
    {
        playerInput?.Disable();
    }

    private void Update()
    {
        UpdateAnimation();
    }

    private void FixedUpdate()
    {
        UpdateMovement();
        Climb();
        UpdateCurserPos();
        if (rb.velocity != Vector2.zero)
        {
            OnMove?.Invoke();
        }
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if(other.gameObject.CompareTag("Ladder"))
        {
            insideLadder = true;
            rb.gravityScale = 0f;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if(other.gameObject.CompareTag("Ladder"))
        {
            insideLadder = false;
            rb.gravityScale = 1f;
        }
    }

    #endregion

    #region Movement

    private void Jump()
    {
        if (GameManager.Instance.IsInteracting || !groundCheck.IsGrounded || groundCheck.PressDown || !canMove) return;
        rb.AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);
        isJump = true;
    }

    private void UpdateMovement()
    {
        UpdateRotate();
        
        if (GameManager.Instance.IsInteracting || isKnockback || !canMove) return;
        
        rb.velocity = new Vector2(moveInput.x * playerSpeed, rb.velocity.y);
        
        UpdateFacing();

        if (!insideLadder)
        {
            
            if (rb.velocity.y < 0)
            {
                rb.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
            }
            else if (rb.velocity.y > 0 && !Input.GetKey(KeyCode.Space))
            {
                rb.velocity += Vector2.up * Physics2D.gravity.y * (holdJumpMultiplier - 1) * Time.deltaTime;
            }
        }

        if (groundCheck.IsGrounded)
        {
            isJump = false;
        }
        
    }

    private void UpdateRotate()
    {
        var _yRot = isFacingRight ? 0f : 180f;
        
        playerTransform.transform.localRotation = Quaternion.Slerp(
            playerTransform.transform.localRotation,
            Quaternion.Euler(0,_yRot,0),
            Time.deltaTime * 30);
    }

    private void UpdateFacing()
    {
        if (moveInput.x >= 1 && !isAttacking)
        {
            isFacingRight = true;
        }
        else if (moveInput.x <= -1 && !isAttacking)
        {
            isFacingRight = false;
        }
    }

    private void Climb()
    {
        if (insideLadder)
        {
            if (moveInput.y != 0)
            {
                rb.velocity = Vector2.up * moveInput.y * climbSpeed;
            }
            else if (moveInput.x == 0)
            {
                rb.velocity = Vector2.zero;
            }
            
        }
    }

    #endregion

    protected void UpdateAnimation()
    {
        //Animation Control
        if (isKnockback) return;
        if (isAttacking) return;
        if (GameManager.Instance.IsInteracting) return;

        if (groundCheck.IsGrounded)
        {
            //if (!canClick) return;
            
            if (moveInput.x != 0)
            {
                if (isOnsecondary) 
                { 
                    ChangeAnimationState(RUN2); 
                }
                    
                ChangeAnimationState(RUN);
            }
            else
            {
                if (isOnsecondary) 
                { 
                    ChangeAnimationState(IDLE2); 
                }

                ChangeAnimationState(IDLE);
            }
        }
        else
        {
            if (isOnsecondary)
            { 
                ChangeAnimationState(JUMP2);
            }

            ChangeAnimationState(JUMP);
        }
        
    }
    
    private void UpdateCurserPos()
    {
        Vector3 _mousePos = mouseInput;

        _mousePos.z = 20;
        _mousePos = cam.ScreenToWorldPoint(_mousePos);
        _mousePos.z = 0;
        curser.position = _mousePos;
    }

    private IEnumerator DelayInput(Action _action)
    {
        yield return new WaitForEndOfFrame();
        if (EventSystem.current.currentSelectedGameObject || !canClick) yield break;
        _action?.Invoke();
        canClick = false;
        StartCoroutine(AbilityCD());

    }
    
    private IEnumerator AbilityCD()
    {
        yield return new WaitForSeconds(abilityCD);
        canClick = true;

    }

    public void ApplyDamage(int _amount, float _pos = 0)
    {
        if(isInvincible) return;
        Knockback(_pos > transform.position.x ? -1 : 1);
        playerSound.PlaySound(playerSound.DamageSound);
        //Deal damage
        hp--;
        OnDamaged?.Invoke();
        if (hp <= 0)
        {
            FindObjectOfType<SceneLoader>().GameOver();
            this.enabled = false;
            return;
        }
        StartCoroutine(InvincibleTime());
        
    }

    private void Knockback(int _direction)
    {
        isKnockback = true;
        rb.velocity = new Vector2(knockbackForce.x * _direction, knockbackForce.y);
        StartCoroutine(KnockbackTime());
    }

    private IEnumerator InvincibleTime()
    {
        var _playerLayer = LayerMask.NameToLayer("Player");
        var _enemyLayer = LayerMask.NameToLayer("Enemy");

        foreach (var _renderer in charContainer.GetComponentsInChildren<SpriteRenderer>())
        {
            _renderer.Blink(invincibleTime);
        }
        
        isInvincible = true;
        Physics.IgnoreLayerCollision(_playerLayer, _enemyLayer, true);
        yield return new WaitForSeconds(invincibleTime);
        isInvincible = false;
        Physics.IgnoreLayerCollision(_playerLayer, _enemyLayer, false);
    }

    private IEnumerator KnockbackTime()
    {
        
        yield return new WaitForSeconds(knockbackTime);
        isKnockback = false;
        
    }
    
    protected void ChangeAnimationState(String _newState, Action _onComplete = null)
    {
        if(currentState == _newState) return;
        
        StopCoroutine("IEChangeAnimation");
        StartCoroutine(IEChangeAnimation(_newState, _onComplete));
        
        
    }

    protected IEnumerator IEChangeAnimation(String _state, Action _oncomplete = null)
    {
        playerAnimator.Play(_state);
        currentState = _state;
        var _clip = FindAnimation(playerAnimator, _state);
        if (_clip == null)
        {
            //Debug.LogErrorFormat("Can't find AnimationClip name {0} in animator.(AnimationClip isn't state name)", _state);
        }
        var _animLength = _clip == null ? 0 : _clip.length;
        abilityCD = _animLength + 0.2f;
        yield return new WaitForSeconds(_animLength);
        _oncomplete?.Invoke();
    }

    private AnimationClip FindAnimation (Animator _animator, string _name) 
    {
        foreach (AnimationClip _clip in _animator.runtimeAnimatorController.animationClips)
        {
            if (_clip.name == _name)
            {
                return _clip;
            }
        }

        return null;
    }
}