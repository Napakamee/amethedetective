using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : Singleton<PoolManager>
{
    public Queue<Bullet> bulletPool = new Queue<Bullet>();
    public override void Init()
    {
        base.Init();
    }

    public void Clear()
    {
        bulletPool.Clear();
    }
}
