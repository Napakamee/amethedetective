using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class TurretEnemy : EnemyBase
{
    [SerializeField] private float attackCd = 1;
    [SerializeField] private float bulletSpeed = 7f;

    
    
    protected override void Start()
    {
        base.Start();
        
    }

    protected override void UpdateFound()
    {
        base.UpdateFound();
        if (canAttack)
        {
            Attack();
        }
    }

    protected override void UpdateMoving()
    {
        FlipRotate();
        rb.velocity = new Vector2(0, rb.velocity.y);
        facingDirection = player.transform.position.x >= transform.position.x ? 1 : -1;
    }

    protected override void EnterKnockback()
    {
        base.EnterKnockback();
        canAttack = false;
        StartCoroutine(AbilityCD());
    }

    private void Attack()
    {
        canAttack = false;
        if (PoolManager.Instance.bulletPool.Count > 1)
        {
            Bullet.SpawnBullet(transform.position, bulletSpeed, facingDirection, BulletType.Enemy);
        }
        else
        {
            Bullet.CreateBullet(transform.position, bulletSpeed, facingDirection, BulletType.Enemy);
        }
        StartCoroutine(AbilityCD());
    }

    private IEnumerator AbilityCD()
    {
        yield return new WaitForSeconds(attackCd);
        canAttack = true;
    }
}
