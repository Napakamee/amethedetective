using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupBase : MonoBehaviour
{
    public Action OnOpen;
    public Action OnClose;

    private void Start()
    {
        OnOpen += delegate
        {
            GameManager.Instance.IsInteracting = true;
        };
        OnClose += delegate
        {
            GameManager.Instance.IsInteracting = false;
        };
    }
}
